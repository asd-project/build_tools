
cmake_minimum_required(VERSION 3.19)

#------------------------------------------------------------------------------

include("${CMAKE_CURRENT_LIST_DIR}/setup.cmake")

include(utils/message)
include(utils/checksum)

include(commands/clean)

include(conan/init)
include(conan/attributes)

#------------------------------------------------------------------------------

function(project_output_dir OUTPUT_DIR project)
    file(RELATIVE_PATH PROJECT_DIR "${WORKSPACE_DIR}" "${project}")
    string(REPLACE ".." "__" PROJECT_DIR "${PROJECT_DIR}")

    set(${OUTPUT_DIR} "${CMAKE_BINARY_DIR}/${PROJECT_DIR}" PARENT_SCOPE)
endfunction()

function(reset_packages editable_packages)
    foreach(entry ${ARGN})
        if(IS_DIRECTORY "${entry}")
            project_output_dir(OUTPUT_DIR "${entry}")

            file(GLOB CONAN_BUILD_FILES LIST_DIRECTORIES false "${OUTPUT_DIR}/conanbuildinfo*" "${OUTPUT_DIR}/*.timestamp" "${OUTPUT_DIR}/*.checksum" "${OUTPUT_DIR}/*.md5" "${OUTPUT_DIR}/*.sha1")

            if(CONAN_BUILD_FILES)
                foreach(FILE IN LISTS CONAN_BUILD_FILES)
                    message("   Remove ${FILE}")
                endforeach()

                message("")

                file(REMOVE ${CONAN_BUILD_FILES})
            endif()

            if(EXISTS "${entry}/conanfile.py" OR EXISTS "${entry}/conanfile.txt")
                if(EXISTS "${entry}/conanfile.py")
                    get_filename_component(absolute_path "${entry}/conanfile.py" ABSOLUTE)
                else()
                    get_filename_component(absolute_path "${entry}/conanfile.txt" ABSOLUTE)
                endif()

                string(REGEX REPLACE "([][+.*()^])" "\\\\\\1" masked_path "${absolute_path}")

                if("${editable_packages}" MATCHES "([.a-zA-Z0-9_-]+)/([.a-zA-Z0-9_-]+)\@([.a-zA-Z0-9_-]+/[.a-zA-Z0-9_-]+)[ \t\r\n]+Path:[ \t\r\n]+${masked_path}[ \t\r\n]+")
                    execute_process(COMMAND conan editable remove "${CMAKE_MATCH_1}/${CMAKE_MATCH_2}@${CMAKE_MATCH_3}" OUTPUT_QUIET WORKING_DIRECTORY "${WORKSPACE_DIR}")
                endif()
            endif()
        endif()
    endforeach()
endfunction()

function(get_checksum_string CONANFILE OUTPUT)
    file(${CHECKSUM_TYPE} "${CONANFILE}" CHECKSUM)
    set(CHECKSUM_STRING "${CHECKSUM}")

    set(PACKAGE_NAMES)

    foreach(PACKAGE IN LISTS CONAN_DEPENDENCIES)
        if(PACKAGE_PATH_${PACKAGE})
            list(APPEND PACKAGE_NAMES "${PACKAGE}")
        endif()
    endforeach()

    list(SORT PACKAGE_NAMES)

    foreach(PACKAGE IN LISTS PACKAGE_NAMES)
        get_conanfile("${PACKAGE_PATH_${PACKAGE}}" PACKAGE_CONANFILE)
        file(${CHECKSUM_TYPE} "${PACKAGE_CONANFILE}" CHECKSUM)
        set(CHECKSUM_STRING "${CHECKSUM_STRING}\n${PACKAGE}: ${CHECKSUM}")
    endforeach()

    set(${OUTPUT} "${CHECKSUM_STRING}" PARENT_SCOPE)
endfunction()

function(conan_collect_dependencies OUTPUT_LIST project)
    project_output_dir(OUTPUT_DIR "${project}")

    if(EXISTS "${OUTPUT_DIR}/conandependencies.txt")
        file(READ "${OUTPUT_DIR}/conandependencies.txt" dependencies)
        set(${OUTPUT_LIST} ${dependencies} PARENT_SCOPE)
        return()
    endif()

    execute_process(COMMAND conan info -n None "${project}" OUTPUT_VARIABLE dependencies WORKING_DIRECTORY "${WORKSPACE_DIR}")
    string(REPLACE "\n" ";" dependencies "${dependencies}")

    file(WRITE "${OUTPUT_DIR}/conandependencies.txt" "${dependencies}")
    set(${OUTPUT_LIST} ${dependencies} PARENT_SCOPE)
endfunction()

#------------------------------------------------------------------------------

function(init_remotes)
    execute_process(COMMAND conan remote list OUTPUT_VARIABLE conan_remotes WORKING_DIRECTORY "${WORKSPACE_DIR}")

    if(NOT "${conan_remotes}" MATCHES "conancenter:")
        execute_process(COMMAND conan remote add conancenter "https://center.conan.io")
        execute_process(COMMAND conan remote add bincrafters "https://bincrafters.jfrog.io/artifactory/api/conan/public-conan")
    endif()
endfunction()

#------------------------------------------------------------------------------

function(regenerate_packages)
    macro(project NAME) # skip compiler check
        _project(${NAME} ${ARGN} LANGUAGES NONE)
    endmacro()

    set(PACKAGE_NAMES)

    foreach(entry IN LISTS DIRECTORIES)
        if(IS_DIRECTORY "${entry}")
            if(EXISTS "${entry}/conanfile.py" OR EXISTS "${entry}/conanfile.txt")
                if(EXISTS "${entry}/conanfile.py")
                    get_filename_component(absolute_path "${entry}/conanfile.py" ABSOLUTE)
                else()
                    get_filename_component(absolute_path "${entry}/conanfile.txt" ABSOLUTE)
                endif()

                get_package_attributes(package "${entry}" "name")
                set(PACKAGE_PATH_${package_name} "${entry}" CACHE PATH "${package_name} local path" FORCE)
                list(APPEND PACKAGE_NAMES "${package_name}")
            endif()
        endif()
    endforeach()

    foreach(PACKAGE IN LISTS PACKAGE_NAMES)
        add_subdirectory("${PACKAGE_PATH_${PACKAGE}}" "${CMAKE_BINARY_DIR}/${PACKAGE}")
    endforeach()
endfunction()

macro(collect_package_info path)
    if(EXISTS "${path}/conanfile.py")
        get_filename_component(absolute_path "${path}/conanfile.py" ABSOLUTE)
    else()
        get_filename_component(absolute_path "${path}/conanfile.txt" ABSOLUTE)
    endif()

    get_package_attributes(package "${path}" "name" "version" "type")
    set(name "${package_name}")
    set(version "${package_version}")
    resolve_package_type(type "${package_type}")

    set(PACKAGE_PATH_${name} "${path}" CACHE PATH "${name} local path" FORCE)
    set(PACKAGE_KEY_${name} "${name}/${version}@${CONAN_USER_CHANNEL}" CACHE PATH "${name} package key" FORCE)

    list(APPEND ROOT_EDITABLES "${PACKAGE_KEY_${name}}")

    file(RELATIVE_PATH RELATIVE_PACKAGE_PATH "${WORKSPACE_DIR}" "${path}")
    list(APPEND PACKAGE_PATHS "${RELATIVE_PACKAGE_PATH}")

    if("${type}" STREQUAL "multi-config" AND "${CMAKE_BINARY_DIR}" MATCHES "${CMAKE_BUILD_TYPE}$")
        get_filename_component(OUTPUT_DIR "${CMAKE_BINARY_DIR}/../Multi/${name}" ABSOLUTE)
    else()
        set(OUTPUT_DIR "${CMAKE_BINARY_DIR}/${name}")
    endif()

    file(RELATIVE_PATH RELATIVE_BUILD_DIR "${path}" "${OUTPUT_DIR}")
    file(RELATIVE_PATH LAYOUT_DIR "${CMAKE_BINARY_DIR}" "${OUTPUT_DIR}")

    if("${type}" STREQUAL "multi-config")
        set(CONAN_BUILD_TYPE_FLAGS ${CONAN_BUILD_TYPE_FLAGS} -s ${name}:build_type=None)

        set(BIN_DIR "")
        set(LIB_DIR "")
    else()
        if ("${BUILD_TYPE}" STREQUAL "Multi")
            set(BIN_DIR "${RELATIVE_BUILD_DIR}/bin/{{settings.get_safe(\"build_type\") | default(\"Release\")}}")
            set(LIB_DIR "${RELATIVE_BUILD_DIR}/lib/{{settings.get_safe(\"build_type\") | default(\"Release\")}}")
        else()
            set(BIN_DIR "${RELATIVE_BUILD_DIR}/bin")
            set(LIB_DIR "${RELATIVE_BUILD_DIR}/lib")
        endif()
    endif()

    configure_file("${BUILD_TOOLS}/templates/workspace_layout.ini" "${OUTPUT_DIR}/layout.ini")

    file(RELATIVE_PATH relative_path "${WORKSPACE_DIR}" "${path}")
    set(EDITABLE_DESCRIPTION "${PACKAGE_KEY_${name}}:\npath: ${relative_path}\nlayout: ${LAYOUT_DIR}/layout.ini")

    list(APPEND WORKSPACE_EDITABLES "${EDITABLE_DESCRIPTION}")
endmacro()

macro(collect_packages_info)
    set(PACKAGE_PATHS)
    set(ROOT_EDITABLES)
    set(WORKSPACE_EDITABLES)

    foreach(path IN LISTS DIRECTORIES)
        if(IS_DIRECTORY "${path}")
            if(EXISTS "${path}/conanfile.py" OR EXISTS "${path}/conanfile.txt")
                collect_package_info("${path}")
            endif()
        endif()
    endforeach()
endmacro()

function(output_targets_mapping)
    set(TARGETS_FILE_CONTENTS "set(PACKAGE_PATHS ${PACKAGE_PATHS})\n")

    foreach(PATH IN LISTS PACKAGE_PATHS)
        if(${PATH}_TARGETS)
            set(TARGETS_FILE_CONTENTS "${TARGETS_FILE_CONTENTS}set(${PATH}_TARGETS ${${PATH}_TARGETS})\n")
            unset("${PATH}_TARGETS" CACHE)
        endif()
    endforeach()

    file(WRITE "${CMAKE_BINARY_DIR}/targets.cmake" "${TARGETS_FILE_CONTENTS}")
endfunction()

#------------------------------------------------------------------------------

function(configure WORKSPACE_DIR)
    set(WORKSPACE_DIR "${WORKSPACE_DIR}" CACHE PATH "Working dir" FORCE)

    set(DIRECTORIES)

    if("${ARGN}" STREQUAL "")
        file(GLOB SUBDIRS "${WORKSPACE_DIR}/*")
    else()
        set(SUBDIRS ${ARGN})
    endif()

    foreach(SUBDIR IN LISTS SUBDIRS)
        collect_subpackages(DIRECTORIES "${SUBDIR}")
    endforeach()

    if("${CONAN_PROFILE}" STREQUAL "")
        set(CONAN_PROFILE "${DEFAULT_CONAN_PROFILE}")
    endif()

    if(REGENERATE_PACKAGES)
        regenerate_packages()
        return()
    endif()

    execute_process(COMMAND conan profile list OUTPUT_VARIABLE profiles RESULT_VARIABLE RESULT WORKING_DIRECTORY "${WORKSPACE_DIR}")

    if(NOT "${profiles}" MATCHES "${CONAN_PROFILE}[\r\n]")
        message(FATAL_ERROR "(${RESULT}) Unknown conan profile: \"${CONAN_PROFILE}\". Available profiles:\n${profiles}")
    endif()

    if("${BUILD_TYPE}" STREQUAL "multi")
        set(CMAKE_BUILD_TYPE "Multi" CACHE STRING "CMake build type" FORCE)
    elseif("${BUILD_TYPE}" STREQUAL "release")
        set(CMAKE_BUILD_TYPE "Release" CACHE STRING "CMake build type" FORCE)
    elseif("${BUILD_TYPE}" STREQUAL "debug")
        set(CMAKE_BUILD_TYPE "Debug" CACHE STRING "CMake build type" FORCE)
    endif()

    init_remotes()

    if(NOT CMAKE_BUILD_TYPE)
        set(CMAKE_BUILD_TYPE Debug CACHE STRING "CMake build type" FORCE)
        message("   Build type is not specified, use ${CMAKE_BUILD_TYPE}")
    endif()

    clean(${DIRECTORIES} PROFILE "${CONAN_PROFILE}" BUILD_TYPE "${CMAKE_BUILD_TYPE}")

    color_message(WHITE "   Collecting packages info...\n")
    collect_packages_info()

#------------------------------------------------------------------------------

    if("${BUILD_TYPE}" STREQUAL "multi")
        set(REQUIRED_BUILD_TYPES Debug Release)
        set(WORKSPACE_PACKAGE_GENERATOR "cmake_multi")
    else()
        set(REQUIRED_BUILD_TYPES ${CMAKE_BUILD_TYPE})
        set(WORKSPACE_PACKAGE_GENERATOR "cmake")
    endif()

    file(RELATIVE_PATH SOURCE_PATH "${CMAKE_BINARY_DIR}" "${WORKSPACE_DIR}")
    set(WORKSPACE_EDITABLES_STRING "    ${WORKSPACE_EDITABLES}")

    string(REPLACE "\n" "\n        " WORKSPACE_EDITABLES_STRING "${WORKSPACE_EDITABLES_STRING}")
    string(REPLACE ";" "\n    " WORKSPACE_EDITABLES_STRING "${WORKSPACE_EDITABLES_STRING}")
    string(REPLACE "path: " "path: ${SOURCE_PATH}/" WORKSPACE_EDITABLES_STRING "${WORKSPACE_EDITABLES_STRING}")

    string(REPLACE ";" ", " ROOT_EDITABLES_STRING "${ROOT_EDITABLES}")

    set(WORKSPACE_FILE_CONTENTS "")
    set(WORKSPACE_FILE_CONTENTS "${WORKSPACE_FILE_CONTENTS}editables:\n${WORKSPACE_EDITABLES_STRING}\n\n")
    set(WORKSPACE_FILE_CONTENTS "${WORKSPACE_FILE_CONTENTS}generators: ${WORKSPACE_PACKAGE_GENERATOR}\n")
    set(WORKSPACE_FILE_CONTENTS "${WORKSPACE_FILE_CONTENTS}workspace_generator: cmake\n")
    set(WORKSPACE_FILE_CONTENTS "${WORKSPACE_FILE_CONTENTS}root: ${ROOT_EDITABLES_STRING}\n")

    file(WRITE "${CMAKE_BINARY_DIR}/workspace.yml" "${WORKSPACE_FILE_CONTENTS}")

    foreach(TYPE IN LISTS REQUIRED_BUILD_TYPES)
        execute_process(COMMAND conan workspace install workspace.yml
            -pr:b=${DEFAULT_CONAN_PROFILE} -pr:h=${CONAN_PROFILE}
            --build=missing
            -c tools.microsoft.msbuild:max_cpu_count=8
            ${CONAN_INSTALL_FLAGS}
            -s build_type=${TYPE} ${CONAN_BUILD_TYPE_FLAGS}
            RESULT_VARIABLE RESULT
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )
    endforeach()

    if(NOT "${RESULT}" EQUAL 0)
        message(FATAL_ERROR "Failed to configure workspace (${RESULT})")
    endif()

    include(${CMAKE_BINARY_DIR}/conanworkspace.cmake)
    conan_workspace_subdirectories()

    output_targets_mapping()

    unset(editable_packages CACHE)
endfunction()
