#--------------------------------------------------------
#   ASD conan attributes access functions
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

include(utils/manifest)

function(generate_package_type output path)
    get_package_attributes(package "${path}" "settings" "requires")

    if("${package_requires}" MATCHES "asd.launch/")
        set(${output} "application" PARENT_SCOPE)
    elseif(NOT "${package_requires}" MATCHES "asd.*/")
        set(${output} "external" PARENT_SCOPE)
    elseif(NOT "${package_settings}" MATCHES "build_type")
        set(${output} "multi-config" PARENT_SCOPE)
    else()
        set(${output} "library" PARENT_SCOPE)
    endif()
endfunction()

function(get_package_type output path)
    if(NOT IS_DIRECTORY "${path}")
        get_filename_component(path "${path}" DIRECTORY)
    endif()

    if(EXISTS "${path}/asd.json")
        manifest_read("${path}/asd.json" MANIFEST)

        if(MANIFEST_type)
            set(${output} "${MANIFEST_type}" PARENT_SCOPE)
            return()
        endif()
    endif()

    generate_package_type(package_type "${path}")

    manifest_set(MANIFEST "type" "\"${package_type}\"")
    manifest_write("${path}/asd.json" MANIFEST)

    set(${output} MANIFEST_type PARENT_SCOPE)
endfunction()

function(get_package_attributes output path)
    set(names ${ARGN})

    if(NOT names)
        message(FATAL_ERROR "get_package_attributes is called without actual attribute names")
    endif()

    if(NOT IS_DIRECTORY "${path}")
        get_filename_component(path "${path}" DIRECTORY)
    endif()

    if(NOT EXISTS "${path}")
        message(FATAL_ERROR "invalid package path: ${path}")
    endif()

    if(EXISTS "${path}/asd.json")
        manifest_read("${path}/asd.json" MANIFEST)
        manifest_set_parent_scope(${output} MANIFEST)

        foreach(KEY IN LISTS MANIFEST_PROPERTIES)
            list(REMOVE_ITEM names "${KEY}")
        endforeach()
    endif()

    if("type" IN_LIST names)
        list(REMOVE_ITEM names "type")
        generate_package_type(value "${path}")

        set(${output}_type ${value} PARENT_SCOPE)
        manifest_set(MANIFEST "type" "\"${value}\"")
    endif()

    if("${names}" STREQUAL "")
        return()
    endif()

    set(arguments)

    foreach(name ${names})
        set(arguments ${arguments} -a ${name})
    endforeach()

    execute_process(
        COMMAND conan inspect . ${arguments}
        OUTPUT_VARIABLE COMMAND_OUTPUT
        ERROR_VARIABLE COMMAND_OUTPUT
        RESULT_VARIABLE RESULT
        WORKING_DIRECTORY "${path}"
    )

    if(NOT ${RESULT} EQUAL 0)
        message(FATAL_ERROR
            "Couldn't get package attributse ${names} for path ${path}: ${COMMAND_OUTPUT} (${RESULT})"
        )
    endif()

    foreach(name ${names})
        string(REGEX MATCH "${name}:[ \t]*([^\n\r]*)" _ "${COMMAND_OUTPUT}")
        string(STRIP "${CMAKE_MATCH_1}" value)

        set(${output}_${name} ${value} PARENT_SCOPE)
        manifest_set(MANIFEST "${name}" "\"${value}\"")
    endforeach()
endfunction()

function(get_profile_attribute attribute profile name)
    execute_process(
        COMMAND conan profile get ${name} ${profile}
        OUTPUT_VARIABLE COMMAND_OUTPUT
        ERROR_VARIABLE COMMAND_OUTPUT
        RESULT_VARIABLE RESULT
    )

    if(NOT ${RESULT} EQUAL 0)
        message(FATAL_ERROR
            "Couldn't get profile attribute ${name} from conan profile ${profile}: ${COMMAND_OUTPUT} (${RESULT})"
        )
    endif()

    string(REGEX MATCH "([^\n]+)" _ "${COMMAND_OUTPUT}")
    string(STRIP "${CMAKE_MATCH_1}" value)

    set(${attribute} ${value} PARENT_SCOPE)
endfunction()
