#--------------------------------------------------------
#   LEGACY: ASD conan package install wrapper
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

function(conan_cleanup OUTPUT_DIR)
    file(GLOB CONAN_BUILD_FILES LIST_DIRECTORIES false "${OUTPUT_DIR}/conanbuildinfo*" "${OUTPUT_DIR}/conandependencies.txt")

    if(CONAN_BUILD_FILES)
        foreach(FILE ${CONAN_BUILD_FILES})
            message("Cleanup ${FILE}")
        endforeach()

        file(REMOVE ${CONAN_BUILD_FILES})
    endif()
endfunction()

function(conan_install OUTPUT_DIR CONANFILE)
	message("Installing conan package...")

    if(NOT CONAN_PROFILE)
        set(CONAN_PROFILE ${DEFAULT_CONAN_PROFILE})
    endif()

    execute_process(COMMAND conan install -if ${OUTPUT_DIR} . -pr:b=${DEFAULT_CONAN_PROFILE} -pr:h=${CONAN_PROFILE} --build missing ${ARGN} ${CONAN_INSTALL_FLAGS} RESULT_VARIABLE RESULT ${QUIET_FLAGS} WORKING_DIRECTORY ${MANIFEST_PATH})

    if(NOT ${RESULT} EQUAL 0)
        conan_cleanup("${OUTPUT_DIR}")
        message("")
        message(FATAL_ERROR "Couldn't install conan manifest")
    endif()

    get_package_type(package_type "${CONANFILE}")

    if(NOT "${package_type}" MATCHES "multi-config" AND "${CMAKE_BUILD_TYPE}" STREQUAL "")
        set(CONAN_BUILD_INFO_FILE "${OUTPUT_DIR}/conanbuildinfo_multi.cmake")
    else()
        set(CONAN_BUILD_INFO_FILE "${OUTPUT_DIR}/conanbuildinfo.cmake")
    endif()

    if(NOT EXISTS "${CONAN_BUILD_INFO_FILE}")
        message(FATAL_ERROR "Conan build info file doesn't exist")
    endif()

    include("${CONAN_BUILD_INFO_FILE}")

    get_filename_component(CONANFILE_NAME "${CONANFILE}" NAME)
    set(CHECKSUM_FILE "${OUTPUT_DIR}/${CONANFILE_NAME}.${CHECKSUM_EXTENSION}")
    get_checksum_string("${CONANFILE}" CHECKSUM_STRING)
    file(WRITE "${CHECKSUM_FILE}" "${CHECKSUM_STRING}")

    command_success("Done!")
endfunction()
