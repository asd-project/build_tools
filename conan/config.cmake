#--------------------------------------------------------
#   ASD conan config access functions
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

function(get_conan_config OUTPUT KEY)
    execute_process(
        COMMAND conan config get ${KEY}
        OUTPUT_VARIABLE COMMAND_OUTPUT
        ERROR_VARIABLE COMMAND_OUTPUT
        RESULT_VARIABLE RESULT
    )

    if(NOT ${RESULT} EQUAL 0)
        message(FATAL_ERROR "Couldn't get ${KEY} from conan config: ${COMMAND_OUTPUT} (${RESULT})")
    endif()

    string(REGEX MATCH "([^\n]+)" _ "${COMMAND_OUTPUT}")
    string(STRIP "${CMAKE_MATCH_1}" value)

    set(${OUTPUT} ${value} PARENT_SCOPE)
endfunction()
