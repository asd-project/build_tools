#--------------------------------------------------------
#   LEGACY: ASD conan package initialization utils
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

include(utils/package)
include(utils/message)

include(conan/install)
include(conan/attributes)

function(get_conanfile PATH OUTPUT)
    if(EXISTS "${PATH}/conanfile.py")
        set(${OUTPUT} "${PATH}/conanfile.py" PARENT_SCOPE)
    elseif(EXISTS "${PATH}/conanfile.txt")
        set(${OUTPUT} "${PATH}/conanfile.txt" PARENT_SCOPE)
    endif()
endfunction()

function(check_conanfile CONANFILE OUTPUT_DIR)
    get_filename_component(CONANFILE_NAME "${CONANFILE}" NAME)
    set(CHECKSUM_FILE "${OUTPUT_DIR}/${CONANFILE_NAME}.${CHECKSUM_EXTENSION}")
    get_checksum_string("${CONANFILE}" CHECKSUM_STRING)

    if(EXISTS "${CHECKSUM_FILE}")
        file(READ "${CHECKSUM_FILE}" SAVED_CHECKSUM)

        if("${SAVED_CHECKSUM}" STREQUAL "${CHECKSUM_STRING}")
            return()
        endif()

        message("${CONANFILE} changed.\nSaved checksum:\n${SAVED_CHECKSUM}\n\nCurrent checksum:\n${CHECKSUM_STRING}")
    endif()

    get_filename_component(SOURCE_DIR "${CONANFILE}" DIRECTORY)

    conan_cleanup("${OUTPUT_DIR}")
    clean("${SOURCE_DIR}")

    file(WRITE "${CHECKSUM_FILE}" "${CHECKSUM_STRING}")
endfunction()

function(conan_init OUTPUT_NAME SOURCE_DIR OUTPUT_DIR)
    if(EXISTS "${SOURCE_DIR}/conanfile.py" OR EXISTS "${SOURCE_DIR}/conanfile.txt")
        set(MANIFEST_PATH ${SOURCE_DIR})
    endif()

    if("${MANIFEST_PATH}" STREQUAL "")
        message(FATAL_ERROR "Couldn't find conanfile.py or conanfile.txt at ${SOURCE_DIR}")
    endif()

    get_conanfile("${MANIFEST_PATH}" CONANFILE)

    if("${CONANFILE}" STREQUAL "")
        message(WARNING "There is no conanfile")
        return()
    endif()

    get_package_attributes(package "${CONANFILE}" "name" "version" "type")

    set(name "${package_name}")
    set(version "${package_version}")
    resolve_package_type(type "${package_type}")

    color_message(CYAN BOLD "${name}/${version}")

    file(READ "${CONANFILE}" MANIFEST_CONTENTS)

    if(NOT "${type}" MATCHES "multi-config" AND "${CMAKE_BUILD_TYPE}" STREQUAL "")
        set(CONAN_BUILD_INFO_FILE "${OUTPUT_DIR}/conanbuildinfo_multi.cmake")
    else()
        set(CONAN_BUILD_INFO_FILE "${OUTPUT_DIR}/conanbuildinfo.cmake")
    endif()

    if(EXISTS "${CONAN_BUILD_INFO_FILE}")
        include("${CONAN_BUILD_INFO_FILE}")
        check_conanfile(${CONANFILE} "${OUTPUT_DIR}")
    endif()

    if(NOT "${type}" MATCHES "multi-config")
        if("${CMAKE_BUILD_TYPE}" STREQUAL "")
            if(NOT EXISTS "${OUTPUT_DIR}/conanbuildinfo_multi.cmake")
                conan_install("${OUTPUT_DIR}" "${CONANFILE}" -g cmake_multi -s build_type=Release)
                conan_install("${OUTPUT_DIR}" "${CONANFILE}" -g cmake_multi -s build_type=Debug)
            endif()
        else()
            if(NOT EXISTS "${OUTPUT_DIR}/conanbuildinfo.cmake")
                conan_install("${OUTPUT_DIR}" "${CONANFILE}" -g cmake -s build_type=${CMAKE_BUILD_TYPE})
            endif()
        endif()
    else()
        if(NOT EXISTS "${OUTPUT_DIR}/conanbuildinfo.cmake")
            conan_install("${OUTPUT_DIR}" "${CONANFILE}" -g cmake -s build_type=Release -s ${name}:build_type=None)
        endif()
    endif()

    set(${OUTPUT_NAME} "${name}" PARENT_SCOPE)
endfunction()
