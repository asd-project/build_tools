include(commands/common)
include(commands/configure)
include(commands/build)

include(utils/args)
include(utils/package)

include(conan/attributes)

function(execute)
    args_parse(
        POSITIONAL
            SUBDIRS...
        OPTIONS
            "+ PROFILES     -p|--profile|PROFILE        = *"
            "+ BUILD_TYPES  -t|--build_type|--build-type|BUILD_TYPE  = *"
            "? CONFIGURE    -c|--configure|CONFIGURE"
            "? BUILD        -b|--build|BUILD"
            "? VERBOSE      --verbose|VERBOSE"
            "? RECURSE      -r|--recurse|RECURSE"
        ARGS ${ARGN}
    )

    if ("${PROFILES}" STREQUAL "*")
        set(PROFILES "${DEFAULT_CONAN_PROFILE}")
    endif()

    if ("${BUILD_TYPES}" STREQUAL "*" OR "${BUILD_TYPES}" STREQUAL "all")
        set(BUILD_TYPES Debug Release)
    else()
        string(REGEX REPLACE "( *,|\\+ *)" ";" BUILD_TYPES "${BUILD_TYPES}")
    endif()

    if ("${SUBDIRS}" STREQUAL "" OR "*" IN_LIST SUBDIRS)
        file(GLOB SUBDIRS RELATIVE "${WORKSPACE_DIR}" "${WORKSPACE_DIR}/*")
    endif()

    foreach(SUBDIR IN LISTS SUBDIRS)
        if (NOT EXISTS "${WORKSPACE_DIR}/${SUBDIR}")
            command_error("Incorrect package path: ${WORKSPACE_DIR}/${SUBDIR}")
        endif()

        if (RECURSE)
            collect_subpackages(DIRECTORIES "${WORKSPACE_DIR}/${SUBDIR}")
        else()
            list(APPEND DIRECTORIES "${WORKSPACE_DIR}/${SUBDIR}")
        endif()
    endforeach()

    function(collect_build_info OUTPUT build_path)
        include("${build_path}/conanbuildinfo.cmake")
        set(${OUTPUT}_DEPENDENCIES ${CONAN_DEPENDENCIES} PARENT_SCOPE)
        set(${OUTPUT}_BUILD_TYPE ${CONAN_SETTINGS_BUILD_TYPE} PARENT_SCOPE)
    endfunction()

    function(collect_packages_list package_name source_path build_path)
        if ("${package_name}" IN_LIST PACKAGES_LIST)
            return()
        endif()

        collect_build_info(INFO "${build_path}")

        foreach(DEPENDENCY IN LISTS INFO_DEPENDENCIES)
            if (PACKAGE_${DEPENDENCY}_BUILD)
                collect_packages_list("${DEPENDENCY}" "${PACKAGE_${DEPENDENCY}_SRC}" "${PACKAGE_${DEPENDENCY}_BUILD}")
            endif()
        endforeach()

        set(PACKAGES_LIST ${PACKAGES_LIST} "${package_name}" CACHE INTERNAL "-" FORCE)

        file(RELATIVE_PATH PACKAGE_PATH "${WORKSPACE_DIR}" "${source_path}")
        set(PACKAGE_PATHS_LIST ${PACKAGE_PATHS_LIST} "${PACKAGE_PATH}" CACHE INTERNAL "-" FORCE)
    endfunction()

    function(commit_package package_name source_path build_path)
        if ("${build_path}" IN_LIST COMMITED_PACKAGES)
            return()
        endif()

        set(COMMITED_PACKAGES ${COMMITED_PACKAGES} "${build_path}" CACHE INTERNAL "-" FORCE)

        command_info("Commit ${package_name}...")

        if (NOT VERBOSE)
            set(OUTPUT_OPTIONS OUTPUT_VARIABLE OUTPUT)
        endif()

        execute_process(
            COMMAND conan export-pkg -if "${build_path}" -bf "${build_path}" --force . ${CONAN_USER_CHANNEL}
            RESULT_VARIABLE RESULT
            ${OUTPUT_OPTIONS}
            WORKING_DIRECTORY "${source_path}"
        )

        if (NOT ${RESULT} EQUAL 0)
            if (NOT VERBOSE)
                file(WRITE "${WORKSPACE_DIR}/commit.log" "${OUTPUT}")
                command_error("Failed to commit ${SUBDIR}, see ${WORKSPACE_DIR}/commit.log(error: ${RESULT})")
            else()
                command_error("Error: ${RESULT}")
            endif()
        endif()
    endfunction()

    set(COMMITED_PACKAGES "" CACHE INTERNAL "-" FORCE)

    if (CONFIGURE)
        set(CONFIGS)

        foreach(PROFILE IN LISTS PROFILES)
            foreach(BUILD_TYPE IN LISTS BUILD_TYPES)
                assume_build_settings(BUILD_DIR BUILD_TYPE "${WORKSPACE_DIR}" "${PROFILE}" "${BUILD_TYPE}" OFF)

                if (NOT EXISTS "${BUILD_DIR}/conanworkspace.cmake")
                    list(APPEND CONFIGS "${PROFILE}/${BUILD_TYPE}")
                endif()
            endforeach()
        endforeach()

        if (CONFIGS)
            string(REPLACE ";" "," CONFIGS "${CONFIGS}")
            configure(CONFIGS "${CONFIGS}")
        endif()
    endif()

    foreach(PROFILE IN LISTS PROFILES)
        foreach(TYPE IN LISTS BUILD_TYPES)
            set(PACKAGES_LIST "" CACHE INTERNAL "-" FORCE)
            set(PACKAGE_PATHS_LIST "" CACHE INTERNAL "-" FORCE)

            assume_build_settings(BUILD_DIR BUILD_TYPE "${WORKSPACE_DIR}" "${PROFILE}" "${TYPE}" OFF)

            command_message(CYAN BOLD
                " * Profile: ${PROFILE}\n"
                " * Build type: ${BUILD_TYPE}"
            )

            if (NOT EXISTS "${BUILD_DIR}/conanworkspace.cmake")
                command_error("${BUILD_DIR}/conanworkspace.cmake not found, may want to call `asd commit -t=${TYPE} -p=${PROFILE} -c` or try to use another build type/profile")
            endif()

            include("${BUILD_DIR}/conanworkspace.cmake")

            foreach(SUBDIR IN LISTS DIRECTORIES)
                if (EXISTS "${SUBDIR}/conanfile.py" OR EXISTS "${SUBDIR}/conanfile.txt")
                    get_package_attributes(package "${SUBDIR}" "name")

                    if (NOT PACKAGE_${package_name}_BUILD)
                        command_error("Package ${package_name} was not configured, try to call `asd configure -t=${TYPE} -p=${PROFILE}` in a package workspace")
                    endif()

                    collect_packages_list("${package_name}" "${PACKAGE_${package_name}_SRC}" "${PACKAGE_${package_name}_BUILD}")
                endif()
            endforeach()

            if (BUILD)
                build("${BUILD_DIR}" "${BUILD_TYPE}" PACKAGES ${PACKAGE_PATHS_LIST})
            endif()

            foreach(PACKAGE IN LISTS PACKAGES_LIST)
                commit_package("${PACKAGE}" "${PACKAGE_${PACKAGE}_SRC}" "${PACKAGE_${PACKAGE}_BUILD}")
            endforeach()
        endforeach()
    endforeach()

    unset(COMMITED_PACKAGES CACHE)

    command_success("Done!")
endfunction()
