
include(commands/build)

include(conan/attributes)

function(find_executable EXECUTABLE_PATH BUILD_DIR TARGET BUILD_TYPE)
    set(CACHE_PATH "${BUILD_DIR}/CMakeCache.txt")

    if(NOT EXISTS "${CACHE_PATH}")
        command_error("Not found cache file at ${CACHE_PATH}")
    endif()

    file(READ "${CACHE_PATH}" CACHE_CONTENTS)
    string(REGEX MATCH "${TARGET}_BINARY_OUTPUT:PATH=([^\n]+)" _ "${CACHE_CONTENTS}")

    if(NOT CMAKE_MATCH_COUNT)
        return()
    endif()

    if(MULTI)
        set(${EXECUTABLE_PATH} "${CMAKE_MATCH_1}/${BUILD_TYPE}/${TARGET}" PARENT_SCOPE)
    else()
        set(${EXECUTABLE_PATH} "${CMAKE_MATCH_1}/${TARGET}" PARENT_SCOPE)
    endif()
endfunction()

function(execute)
    args_separate(
        BUILD_OPTIONS BUILD_TOOL_OPTIONS RUN_OPTIONS
        ARGS ${ARGN}
    )

    args_parse(
        POSITIONAL
            TARGET
        OPTIONS
            "= PROFILE      -p|--profile|PROFILE        = *"
            "= BUILD_TYPE   -t|--build_type|--build-type|BUILD_TYPE  = debug"
            "= CONFIGURE    -c|--configure|CONFIGURE"
            "? SKIP_BUILD   -f|--skip_build|SKIP_BUILD"
            "? MULTI        -m|--multi|MULTI"
        ARGS ${BUILD_OPTIONS}
    )

    if("${PROFILE}" STREQUAL "*")
        set(PROFILE "${DEFAULT_CONAN_PROFILE}")
    endif()

    assume_build_settings(BUILD_DIR BUILD_TYPE "${WORKSPACE_DIR}" "${PROFILE}" "${BUILD_TYPE}" ${MULTI})

    if(CONFIGURE)
        string(TOUPPER "${CONFIGURE}" CONFIGURE)

        if("${CONFIGURE}" STREQUAL "TRUE" OR "${CONFIGURE}" STREQUAL "FORCE")
            configure(CONFIG "${PROFILE}/${BUILD_TYPE}")
        elseif(NOT "${CONFIGURE}" STREQUAL "MISSING" AND NOT "${CONFIGURE}" STREQUAL "FALSE")
            command_error("Incorrect value for \"CONFIGURE\" flag - \"${CONFIGURE}\", expected TRUE/FORCE, MISSING OR FALSE")
        endif()
    endif()

    if(NOT EXISTS "${BUILD_DIR}")
        if(NOT CONFIGURE)
            command_error("There is no build directory for the specified settings, run `asd configure -t=${BUILD_TYPE} -p=${PROFILE}`")
        endif()

        command_info("Missing configuration: ${PROFILE}/${BUILD_TYPE}")
        configure(CONFIG "${PROFILE}/${BUILD_TYPE}")
    endif()

    find_executable(EXECUTABLE_PATH "${BUILD_DIR}" "${TARGET}" "${BUILD_TYPE}")

    if (NOT EXECUTABLE_PATH)
        if(NOT CONFIGURE)
            command_error("Not found ${TARGET} binary output path at ${BUILD_DIR}/CMakeCache.txt")
        endif()

        configure(CONFIG "${PROFILE}/${BUILD_TYPE}")
    endif()

    if(NOT SKIP_BUILD)
        build("${BUILD_DIR}" "${BUILD_TYPE}" TARGET "${TARGET}")
        message("")
    endif()

    if(RUN_OPTIONS)
        string(REPLACE ";" "\" \"" RUN_OPTIONS_STRING " \"${RUN_OPTIONS}\"")
    endif()

    command_message(CYAN BOLD "Running ${EXECUTABLE_PATH}${RUN_OPTIONS_STRING}...")

    get_profile_attribute(CONAN_SETTINGS_OS ${PROFILE} settings.os)

    if("${CONAN_SETTINGS_OS}" STREQUAL "Emscripten")
        if("$ENV{EMSCRIPTEN}" STREQUAL "")
            command_error("Trying to launch emscripten executable, but environment variable EMSCRIPTEN is not defined")
        endif()

        command_message(CYAN BOLD "$ENV{EMSCRIPTEN}/emrun --browser=chrome --browser_args='--user-data-dir=\"/tmp/emrun\"' ${EXECUTABLE_PATH}.html")
        execute_process(COMMAND "$ENV{EMSCRIPTEN}/emrun" "--browser=chrome" "--browser_args='--user-data-dir=\"/tmp/emrun\"'" "${EXECUTABLE_PATH}.html" WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}" RESULT_VARIABLE RESULT ENCODING UTF8)
    elseif(WIN32)
        execute_process(COMMAND cmd /c "${EXECUTABLE_PATH}" ${RUN_OPTIONS} WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}" RESULT_VARIABLE RESULT ENCODING UTF8)
    else()
        execute_process(COMMAND "${EXECUTABLE_PATH}" ${RUN_OPTIONS} WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}" RESULT_VARIABLE RESULT ENCODING UTF8)
    endif()

    if(NOT ${RESULT} EQUAL 0)
        command_error("Error: ${RESULT}")
    endif()
endfunction()
