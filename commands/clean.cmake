
include(utils/package)
include(utils/message)

include(conan/attributes)

include(commands/common)

include(utils/args)

function(clean)
    args_parse(
        POSITIONAL
            SUBDIRS...
        OPTIONS
            "= PROFILE      -p|--profile|PROFILE        = *"
            "= BUILD_TYPE   -t|--build_type|--build-type|BUILD_TYPE  = *"
            "? VERBOSE      --verbose|VERBOSE"
        ARGS ${ARGN}
    )

    set(PACKAGES)

    foreach(SUBDIR IN LISTS SUBDIRS)
        if(EXISTS "${SUBDIR}/conanfile.py" OR EXISTS "${SUBDIR}/conanfile.txt")
            list(APPEND PACKAGES "${SUBDIR}")
        endif()
    endforeach()

    if(VERBOSE)
        string(REPLACE ";" "\n" PACKAGES_LIST "${PACKAGES}")
        color_message(WHITE BOLD "Clean packages:")
        message("${PACKAGES_LIST}")
    endif()

    foreach(package IN LISTS PACKAGES)
        foreach(dir "bin" "lib")
            if(EXISTS "${package}/${dir}")
                if(PROFILE AND NOT "${PROFILE}" STREQUAL "*")
                    set(dir "${dir}/${PROFILE}")
                endif()

                if(BUILD_TYPE AND NOT "${BUILD_TYPE}" STREQUAL "*")
                    file(GLOB contents LIST_DIRECTORIES true "${package}/${dir}/${BUILD_TYPE}/*")

                    foreach(entry ${contents})
                        if(VERBOSE)
                            message("Remove ${entry}")
                        endif()

                        file(REMOVE_RECURSE "${entry}")
                    endforeach()
                else()
                    if(EXISTS "${package}/${dir}")
                        if(VERBOSE)
                            message("Remove ${package}/${dir}")
                        endif()

                        file(REMOVE_RECURSE "${package}/${dir}")
                    endif()
                endif()
            endif()
        endforeach()
    endforeach()

    if(VERBOSE)
        command_success("Done!")
    endif()
endfunction()

function(execute)
    args_parse(
        POSITIONAL
            SUBDIRS...
        OPTIONS
            "+ PROFILES     -p|--profile|PROFILE        = *"
            "+ BUILD_TYPES  -t|--build_type|--build-type|BUILD_TYPE  = *"
        ARGS ${ARGN}
    )

    if(NOT "${BUILD_TYPES}" STREQUAL "*")
        string(REGEX REPLACE "( *,|\\+ *)" ";" BUILD_TYPES "${BUILD_TYPES}")
    endif()

    if("${SUBDIRS}" STREQUAL "" OR "*" IN_LIST SUBDIRS)
        file(GLOB SUBDIRS RELATIVE "${WORKSPACE_DIR}" "${WORKSPACE_DIR}/*")
    endif()

    set(DIRECTORIES)

    foreach(SUBDIR IN LISTS SUBDIRS)
        if(NOT EXISTS "${WORKSPACE_DIR}/${SUBDIR}")
            command_error("Incorrect package path: ${WORKSPACE_DIR}/${SUBDIR}")
        endif()

        collect_subpackages(DIRECTORIES "${WORKSPACE_DIR}/${SUBDIR}")
    endforeach()

    foreach(PROFILE IN LISTS PROFILES)
        foreach(BUILD_TYPE IN LISTS BUILD_TYPES)
            clean(${DIRECTORIES} PROFILE "${PROFILE}" BUILD_TYPE "${BUILD_TYPE}" VERBOSE)
        endforeach()
    endforeach()
endfunction()
