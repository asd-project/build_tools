
include(utils/message)

if(NOT DEFINED SHOW_COMMAND_OUTPUT)
    set(SHOW_COMMAND_OUTPUT ON)
endif()

function(emit_compile_flags COMPILE_COMMANDS_FILE)
    file(READ "${COMPILE_COMMANDS_FILE}" CONTENTS)
    string(REGEX MATCHALL "\"command\": \"([^\n]+)\",?\n" COMMANDS "${CONTENTS}")
    set(FLAGS)
    set(OUTPUT)

    foreach(COMMAND ${COMMANDS})
        string(REGEX MATCH "\"command\": \"([^\n]+)\",?\n" COMMAND "${COMMAND}")
        string(REPLACE "-isystem " "-isystem_" COMMAND "${CMAKE_MATCH_1}")
        string(REGEX REPLACE " -o .*$" "" COMMAND "${COMMAND}")

        separate_arguments(ARGS UNIX_COMMAND "${COMMAND}")
        list(REMOVE_AT ARGS 0) # remove compile command

        list(APPEND FLAGS ${ARGS})
        list(REMOVE_DUPLICATES FLAGS)
    endforeach()

    foreach(FLAG ${FLAGS})
        string(REPLACE "-isystem_" "-isystem\n" FLAG "${FLAG}")
        set(OUTPUT "${OUTPUT}${FLAG}\n")
    endforeach()

    file(WRITE "${CMAKE_SOURCE_DIR}/compile_flags.txt" "-x\nc++\n${OUTPUT}-E\n-CC\n")
endfunction()

function(command_message)
    if(SHOW_COMMAND_OUTPUT)
        color_message(${ARGN})
    endif()
endfunction()

function(command_info)
    command_message(WHITE BOLD "   " ${ARGN})
endfunction()

function(command_success)
    message("")
    command_message(GREEN BOLD "${MESSAGE_SUCCESS_STATUS} " ${ARGN})
endfunction()

function(command_error)
    message("")
    color_message(RED BOLD "${MESSAGE_WARNING_STATUS} " ${ARGN})
    message("")
    message(FATAL_ERROR)
endfunction()

function(assume_build_settings BUILD_DIR ACTUAL_BUILD_TYPE WORKSPACE_DIR PROFILE BUILD_TYPE MULTI)
    if(MULTI)
        set(${ACTUAL_BUILD_TYPE} "Multi" PARENT_SCOPE)
        set(BUILD_DIR "${WORKSPACE_DIR}/build/${PROFILE}/Multi" PARENT_SCOPE)
        return()
    endif()

    string(TOLOWER "${BUILD_TYPE}" build_type)

    if("${build_type}" STREQUAL "release")
        set(BUILD_TYPE Release)
    elseif("${build_type}" STREQUAL "debug")
        set(BUILD_TYPE Debug)
    else()
        command_error("Unknown build type: ${BUILD_TYPE}")
    endif()

    set(${ACTUAL_BUILD_TYPE} "${BUILD_TYPE}" PARENT_SCOPE)
    set(BUILD_DIR "${WORKSPACE_DIR}/build/${PROFILE}/${BUILD_TYPE}" PARENT_SCOPE)
endfunction()
