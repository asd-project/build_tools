
include(commands/common)

include(utils/args)

include(conan/attributes)

set(CMAKE_BINARY_DIR "${WORKSPACE_DIR}/build/.tmp")

function(update_packages CONAN_PROFILE BUILD_TYPE MULTI)
    assume_build_settings(BUILD_DIR ACTUAL_BUILD_TYPE "${WORKSPACE_DIR}" "${CONAN_PROFILE}" "${BUILD_TYPE}" ${MULTI})

    if(NOT EXISTS "${BUILD_DIR}")
        message(FATAL_ERROR "Can't update packages - workspace has not been configured for ${CONAN_PROFILE}/${BUILD_TYPE} yet")
    endif()

    file(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}")

    set(PACKAGES)

    if (NOT ARGN)
        color_message(BOLD WHITE "   Updating all packages...\n")
    else()
        set(UPDATE_MESSAGE BOLD WHITE "   Updating packages: ")

        foreach(PACKAGE ${ARGN})
            if (PACKAGES)
                list(APPEND UPDATE_MESSAGE WHITE ", ")
            endif()

            list(APPEND UPDATE_MESSAGE CYAN "${PACKAGE}")
            list(APPEND PACKAGES "${WORKSPACE_DIR}/${PACKAGE}")
        endforeach()

        color_message(${UPDATE_MESSAGE} WHITE "...\n")
    endif()

    file(RELATIVE_PATH RELATIVE_WORKSPACE_DIR "${CMAKE_BINARY_DIR}" "${WORKSPACE_DIR}")

    execute_process(COMMAND ${CMAKE_COMMAND} "${RELATIVE_WORKSPACE_DIR}"
        "-DREGENERATE_PACKAGES=TRUE"
        "-DREGENERATE_ROOT=${BUILD_DIR}"
        "-DCHECK_UNLISTED_FILES=${CHECK}"
        "-DREMOVE_UNLISTED_FILES=${CLEANUP}"
        "-DFORCE_COLORIZE=${FORCE_COLORIZE}"
        "-DROOT_PATH=${ROOT}"
        "-DPACKAGES=${PACKAGES}"
        ${OUTPUT_OPTIONS}
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
        RESULT_VARIABLE RESULT
        ENCODING UTF8
    )

    file(REMOVE_RECURSE "${CMAKE_BINARY_DIR}")

    if(NOT "${RESULT}" EQUAL 0)
        command_error("Failed to update project")
    endif()

    command_success("Done!")
endfunction()

function(execute)
    args_parse(
        POSITIONAL
            PACKAGES...
        OPTIONS
            "+ PROFILES     -p|--profile|--profiles|PROFILE|PROFILES = *"
            "+ BUILD_TYPES  -t|--build_type|--build_types|--build-type|--build-types|BUILD_TYPE|BUILD_TYPES = debug"
            "+ CONFIGS      --config|--configs|CONFIG|CONFIGS"
            "? MULTI        -m|--multi|MULTI"
            "? CHECK        -c|--check|CHECK"
            "? CLEANUP      --cleanup|CLEANUP"
        ARGS ${ARGN}
    )

    if(NOT CONFIGS)
        if("${PROFILES}" STREQUAL "*")
            set(PROFILES "${DEFAULT_CONAN_PROFILE}")
        endif()

        if(MULTI)
            set(BUILD_TYPES Multi)
        elseif("${BUILD_TYPES}" STREQUAL "*" OR "${BUILD_TYPES}" STREQUAL "all")
            set(BUILD_TYPES Debug Release)
        else()
            string(REGEX REPLACE "( *,|\\+ *)" ";" BUILD_TYPES "${BUILD_TYPES}")
        endif()

        set(CONFIGS)

        foreach(PROFILE IN LISTS PROFILES)
            foreach(BUILD_TYPE IN LISTS BUILD_TYPES)
                list(APPEND CONFIGS "${PROFILE}/${BUILD_TYPE}")
            endforeach()
        endforeach()
    else()
        string(REGEX REPLACE "( *,|\\+ *)" ";" CONFIGS "${CONFIGS}")
    endif()

    string(REPLACE ";" ", " CONFIGS_DESCRIPTION "${CONFIGS}")

    set(OPTIONS_DESCRIPTION
        " * Configs: ${CONFIGS_DESCRIPTION}"
    )

    string(REPLACE ";" "\n" OPTIONS_DESCRIPTION "${OPTIONS_DESCRIPTION}")
    command_message(CYAN BOLD "${OPTIONS_DESCRIPTION}\n")

    foreach(CONFIG IN LISTS CONFIGS)
        string(REPLACE "/" ";" CONFIG_PARTS "${CONFIG}")
        list(POP_FRONT CONFIG_PARTS PROFILE BUILD_TYPE)

        if (NOT BUILD_TYPE)
            command_error("Invalid config key: ${CONFIG}")
        endif()

        string(TOLOWER ${BUILD_TYPE} BUILD_TYPE)
        update_packages("${PROFILE}" "${BUILD_TYPE}" "${MULTI}" ${PACKAGES})
    endforeach()
endfunction()
