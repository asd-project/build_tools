#--------------------------------------------------------
#   create script
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.3)

#--------------------------------------------------------

include(commands/common)

include(utils/args)

#--------------------------------------------------------

set(ALIASES_APP "APPLICATION")
set(ALIASES_LIB "LIBRARY")

function(create)
    args_parse(
        POSITIONAL
             PACKAGE_TYPE NAME
        OPTIONS
            "+ URL      ---url|URL"
            "+ BASE     -b|--base|BASE"
            "? UPDATE   -u|--update|UPDATE"
            "? FORCE    -f|--force|FORCE"
        ARGS ${ARGN}
    )

    function(camel_case str out)
        set(result "")
        string(REPLACE "_" ";" components "${str}")

        foreach(c ${components})
            string(TOLOWER "${c}" L)

            string(SUBSTRING "${L}" 0 1 F)
            string(SUBSTRING "${L}" 1 -1 T)
            string(TOUPPER "${F}" U)

            set(result "${result}${U}${T}")
        endforeach()

        set(${out} "${result}" PARENT_SCOPE)
    endfunction()

    string(TOUPPER "${PACKAGE_TYPE}" PACKAGE_TYPE)

    if(ALIASES_${PACKAGE_TYPE})
        set(PACKAGE_TYPE "${ALIASES_${PACKAGE_TYPE}}")
    endif()

    get_filename_component(package_name "${NAME}" NAME)
    string(TOLOWER "${PACKAGE_TYPE}" package_type)
    string(TOUPPER "${PACKAGE_TYPE}" PACKAGE_TYPE)
    camel_case("${package_name}" PackageName)

    set(PACKAGE_DIR "${NAME}")
    set(PACKAGE_TEMPLATE_DIR "${BUILD_TOOLS}/templates/${package_type}")

    command_info("Setting package parameters...")

    if(NOT EXISTS "${PACKAGE_TEMPLATE_DIR}")
        command_error("Unknown package type: ${package_type}")
    endif()

    if("${package_type}" STREQUAL "external")
        if(NOT URL)
            command_error("Need package url for external package! Try `asd create <name> external --url <url>`")
        endif()

        list(package_url URL)
    elseif("${package_type}" STREQUAL "test")
        if(BASE)
            set(PACKAGE_DIR "${BASE}/tests/${NAME}")
        endif()
    elseif("${package_type}" STREQUAL "example")
        if(NOT BASE)
            command_error("Need base package name for example! Try `asd create <name> example --base <base-package-name>`")
        endif()

        set(PACKAGE_DIR "${BASE}/examples/${NAME}")
    elseif("${package_type}" STREQUAL "benchmark")
        if(BASE)
            set(PACKAGE_DIR "${BASE}/benchmarks/${NAME}")
        endif()
    endif()

    set(FULL_PACKAGE_DIR "${WORKSPACE_DIR}/${PACKAGE_DIR}")

    if(EXISTS "${FULL_PACKAGE_DIR}")
        if(FORCE)
            file(REMOVE_RECURSE "${FULL_PACKAGE_DIR}")
        else()
            command_error("Directory ${FULL_PACKAGE_DIR} already exists. Use \"asd create -f ...\" to recreate package directory")
        endif()
    endif()

    file(MAKE_DIRECTORY "${FULL_PACKAGE_DIR}")

    command_info("Populating package directory...")

    configure_file("${PACKAGE_TEMPLATE_DIR}/.gitignore" "${FULL_PACKAGE_DIR}/.gitignore" @ONLY)
    configure_file("${PACKAGE_TEMPLATE_DIR}/asd.json" "${FULL_PACKAGE_DIR}/asd.json" @ONLY)
    configure_file("${PACKAGE_TEMPLATE_DIR}/CMakeLists.txt" "${FULL_PACKAGE_DIR}/CMakeLists.txt" @ONLY)
    configure_file("${PACKAGE_TEMPLATE_DIR}/conanfile.py" "${FULL_PACKAGE_DIR}/conanfile.py" @ONLY)

    command_success("Created package \"${package_name}\" at ${FULL_PACKAGE_DIR}")

    if(UPDATE)
        message("")
        include(commands/update)
        update_packages("${PACKAGE_DIR}")
    endif()
endfunction()

function(execute)
    create(${ARGN})
endfunction()
