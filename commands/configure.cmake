
#------------------------------------------------------------------------------

include(commands/common)

include(utils/args)

include(conan/attributes)

#------------------------------------------------------------------------------

set(GENERATOR_ALIAS_MAKE "Unix Makefile")
set(GENERATOR_ALIAS_XCODE "Xcode")
set(GENERATOR_ALIAS_VS14 "Visual Studio 14")
set(GENERATOR_ALIAS_VS15 "Visual Studio 15")
set(GENERATOR_ALIAS_VS16 "Visual Studio 16")

#------------------------------------------------------------------------------

macro(init_toolchain CONAN_PROFILE BUILD_TYPE MULTI)
    assume_build_settings(BUILD_DIR ACTUAL_BUILD_TYPE "${WORKSPACE_DIR}" "${CONAN_PROFILE}" "${BUILD_TYPE}" ${MULTI})

    set(TOOLCHAIN_INIT_DIR "${BUILD_DIR}/.toolchain")

    if(NOT EXISTS "${BUILD_DIR}")
        file(MAKE_DIRECTORY "${BUILD_DIR}")
    endif()

    if(NOT EXISTS "${TOOLCHAIN_INIT_DIR}")
        file(MAKE_DIRECTORY "${TOOLCHAIN_INIT_DIR}")
    endif()

    if(NOT MULTI)
        set(BUILD_TYPE_SETTING -s build_type=${ACTUAL_BUILD_TYPE})
    endif()

    if(VERBOSE)
        command_message(CYAN BOLD "   Init toolchain at ${TOOLCHAIN_INIT_DIR}...")
        execute_process(COMMAND conan install -if "${TOOLCHAIN_INIT_DIR}" -pr:b=${DEFAULT_CONAN_PROFILE} -pr:h=${CONAN_PROFILE} --build=missing ${BUILD_TYPE_SETTING} . RESULT_VARIABLE RESULT WORKING_DIRECTORY "${ROOT}/build_tools")

        if(NOT "${RESULT}" EQUAL 0)
            command_error("Couldn't init toolchain at ${TOOLCHAIN_INIT_DIR}: ${RESULT}")
        endif()
    else()
        execute_process(COMMAND conan install -if "${TOOLCHAIN_INIT_DIR}" -pr:b=${DEFAULT_CONAN_PROFILE} -pr:h=${CONAN_PROFILE} --build=missing ${BUILD_TYPE_SETTING} . OUTPUT_VARIABLE OUTPUT ERROR_VARIABLE OUTPUT RESULT_VARIABLE RESULT WORKING_DIRECTORY "${ROOT}/build_tools")

        if(NOT "${RESULT}" EQUAL 0)
            command_error("Couldn't init toolchain at ${TOOLCHAIN_INIT_DIR}: ${OUTPUT} (${RESULT})")
        endif()
    endif()

    file(READ "${TOOLCHAIN_INIT_DIR}/environment.sh.env" ENV_VARS)

    string(REGEX REPLACE ";" "\\\\;" ENV_VARS "${ENV_VARS}")
    string(REGEX REPLACE "\n" ";" ENV_VARS "${ENV_VARS}")

    set(ENVIRONMENT)

    foreach(ENV_VAR ${ENV_VARS})
        if("${ENV_VAR}" MATCHES "([^=]+)=(\".*\")")
            set(ENV_NAME ${CMAKE_MATCH_1})
            string(REGEX REPLACE ":" ";" COMPONENTS "${CMAKE_MATCH_2}")
            set(VALUE)

            foreach(COMPONENT ${COMPONENTS})
                if("${COMPONENT}" MATCHES "\"([^\"]+)\"")
                    list(APPEND VALUE "${CMAKE_MATCH_1}")
                endif()
            endforeach()

            string(REGEX REPLACE ";" ":" VALUE "${VALUE}")

            if("${ENV_NAME}" STREQUAL "PATH")
                set(ENV{PATH} "${VALUE}:$ENV{PATH}")
                set(ENVIRONMENT "${ENVIRONMENT}set(ENV{${ENV_NAME}} \"${VALUE}:\$ENV{PATH}\")\n")
            else()
                set(ENV{${ENV_NAME}} "${VALUE}")
                set(ENVIRONMENT "${ENVIRONMENT}set(ENV{${ENV_NAME}} \"${VALUE}\")\n")
            endif()
        endif()
    endforeach()

    file(WRITE "${BUILD_DIR}/env.cmake" "${ENVIRONMENT}")
endmacro()

function(configure)
    args_parse(
        OPTIONS
            "+ PROFILES         -p|--profile|--profiles|PROFILE|PROFILES = *"
            "+ BUILD_TYPES      -t|--build_type|--build_types|--build-type|--build-types|BUILD_TYPE|BUILD_TYPES = debug"
            "+ CONFIGS          --config|--configs|CONFIG|CONFIGS"
            "= GENERATOR        -g|GENERATOR"
            "+ RESET_PACKAGES   -r|--reset|RESET"
            "+ REBUILD          -b|--rebuild|REBUILD"
            "? MULTI            -m|--multi|MULTI"
            "? UNITY            -u|--unity|UNITY"
            "? CHECK            -c|--check|CHECK"
            "? CLEANUP          --cleanup|CLEANUP"
            "? VERBOSE          --verbose|VERBOSE"
        ARGS ${ARGN}
    )

    #------------------------------------------------------------------------------

    if(NOT CONFIGS)
        if("${PROFILES}" STREQUAL "*")
            set(PROFILES "${DEFAULT_CONAN_PROFILE}")
        endif()

        if(MULTI)
            set(BUILD_TYPES Multi)
        elseif("${BUILD_TYPES}" STREQUAL "*" OR "${BUILD_TYPES}" STREQUAL "all")
            set(BUILD_TYPES Debug Release)
        else()
            string(REGEX REPLACE "( *,|\\+ *)" ";" BUILD_TYPES "${BUILD_TYPES}")
        endif()

        set(CONFIGS)

        foreach(PROFILE IN LISTS PROFILES)
            foreach(BUILD_TYPE IN LISTS BUILD_TYPES)
                list(APPEND CONFIGS "${PROFILE}/${BUILD_TYPE}")
            endforeach()
        endforeach()
    else()
        string(REGEX REPLACE "( *,|\\+ *)" ";" CONFIGS "${CONFIGS}")
    endif()

    #------------------------------------------------------------------------------

    set(COMMON_OPTIONS
        "-DCMAKE_MODULE_PATH=${ROOT}/build_tools"
        "${CONAN_CMAKE_TOOLCHAIN_FILE}"
        "${RESET_PACKAGES}"
        "${CONAN_INSTALL_FLAGS}"
        "-DCMAKE_UNITY_BUILD=${UNITY}"
        "-DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
        "-DFORCE_COLORIZE=${FORCE_COLORIZE}"
        "-DCHECK_UNLISTED_FILES=${CHECK}"
        "-DREMOVE_UNLISTED_FILES=${CLEANUP}"
    )

    if(DEFINED ENV{CONAN_CMAKE_TOOLCHAIN_FILE})
        list(APPEND COMMON_OPTIONS "-DCMAKE_TOOLCHAIN_FILE=$ENV{CONAN_CMAKE_TOOLCHAIN_FILE}")
    endif()

    if(RESET_PACKAGES)
        string(REPLACE "," ";" RESET_PACKAGES ${RESET_PACKAGES})
        list(APPEND COMMON_OPTIONS "-DRESET_PACKAGES=${RESET_PACKAGES}")
    endif()

    if(REBUILD_PACKAGES)
        set(CONAN_INSTALL_FLAGS)

        string(REPLACE "," ";" REBUILD_PACKAGES ${REBUILD_PACKAGES})

        foreach(PACKAGE IN LISTS REBUILD_PACKAGES)
            if("${PACKAGE}" STREQUAL "*")
                set(CONAN_INSTALL_FLAGS "--build=*")
                break()
            endif()

            list(APPEND CONAN_INSTALL_FLAGS "--build=${PACKAGE}/*")
        endforeach()

        list(APPEND COMMON_OPTIONS "-DCONAN_INSTALL_FLAGS=${CONAN_INSTALL_FLAGS}")
    endif()

    if(NOT GENERATOR AND DEFINED ENV{CONAN_CMAKE_GENERATOR})
        set(GENERATOR $ENV{CONAN_CMAKE_GENERATOR})
    endif()

    if(GENERATOR)
        string(TOUPPER "${GENERATOR}" GENERATOR_KEY)
        set(GENERATOR_ALIAS "${GENERATOR_ALIAS_${GENERATOR_KEY}}")

        if(NOT "${GENERATOR_ALIAS}" STREQUAL "")
            set(GENERATOR "${GENERATOR_ALIAS}")
        endif()

        list(APPEND COMMON_OPTIONS -G "${GENERATOR}")
    endif()

    #------------------------------------------------------------------------------

    string(REPLACE ";" ", " CONFIGS_DESCRIPTION "${CONFIGS}")

    set(OPTIONS_DESCRIPTION
        " * Configs: ${CONFIGS_DESCRIPTION}"
    )

    if(UNITY)
        list(APPEND OPTIONS_DESCRIPTION " * Unity build: on")
    else()
        list(APPEND OPTIONS_DESCRIPTION " * Unity build: off")
    endif()

    string(REPLACE ";" "\n" OPTIONS_DESCRIPTION "${OPTIONS_DESCRIPTION}")
    command_message(CYAN BOLD "${OPTIONS_DESCRIPTION}\n")

    set(COMMANDS)

    #------------------------------------------------------------------------------

    foreach(CONFIG IN LISTS CONFIGS)
        string(REPLACE "/" ";" CONFIG_PARTS "${CONFIG}")
        list(POP_FRONT CONFIG_PARTS PROFILE BUILD_TYPE)

        if (NOT BUILD_TYPE)
            command_error("Invalid config key: ${CONFIG}")
        endif()

        string(TOLOWER ${BUILD_TYPE} BUILD_TYPE)
        init_toolchain("${PROFILE}" "${BUILD_TYPE}" ${MULTI})

        set(COMMANDS ${COMMANDS} COMMAND ${CMAKE_COMMAND}
            ${COMMON_OPTIONS}
            "-DCONAN_PROFILE=${PROFILE}"
            "-DBUILD_TYPE=${BUILD_TYPE}"
            "-S${WORKSPACE_DIR}"
            "-B${BUILD_DIR}"
        )
    endforeach()

    execute_process(
        ${COMMANDS}
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
        RESULT_VARIABLE RESULT
    )

    if(NOT ${RESULT} EQUAL 0)
        return()
    endif()

    #------------------------------------------------------------------------------

    if(EXISTS "${BUILD_DIR}/compile_commands.json")
        file(COPY "${BUILD_DIR}/compile_commands.json" DESTINATION "${CMAKE_SOURCE_DIR}")
        command_message(CYAN BOLD "   Copied compile_commands.json")

        emit_compile_flags("${BUILD_DIR}/compile_commands.json")
        command_message(CYAN BOLD "   Generated compile_flags.txt at ${CMAKE_SOURCE_DIR}")
    endif()
endfunction()

function(execute)
    configure(${ARGN})
    command_success("Done!")
endfunction()
