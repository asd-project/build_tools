
include(commands/common)
include(commands/configure)

include(utils/args)
include(utils/package)

include(conan/attributes)

function(build BUILD_DIR BUILD_TYPE)
    cmake_parse_arguments(CONTEXT "RECURSE" "BULK;TARGET;PACKAGE" "TARGETS;PACKAGES" ${ARGN})

    set(TARGETS ${CONTEXT_TARGET} ${CONTEXT_TARGETS})
    set(PACKAGES ${CONTEXT_PACKAGE} ${CONTEXT_PACKAGES})

    if(PACKAGES)
        if(NOT EXISTS "${BUILD_DIR}/targets.cmake")
            command_error("Can not build package targets, ${BUILD_DIR}/targets.cmake does not exist")
        endif()

        include("${BUILD_DIR}/targets.cmake")

        foreach(PACKAGE IN LISTS PACKAGES)
            if(CONTEXT_RECURSE)
                collect_subpackages(SUBPACKAGES "${WORKSPACE_DIR}/${PACKAGE}")

                foreach(SUBPACKAGE IN LISTS SUBPACKAGES)
                    file(RELATIVE_PATH SUBPACKAGE "${WORKSPACE_DIR}" "${SUBPACKAGE}")
                    list(APPEND TARGETS ${${SUBPACKAGE}_TARGETS})
                endforeach()
            else()
                list(APPEND TARGETS ${${PACKAGE}_TARGETS})
            endif()
        endforeach()
    endif()

    if("${TARGETS}" STREQUAL "")
        set(TARGETS "*")
    endif()

    if(EXISTS "${BUILD_DIR}/compile_commands.json")
        file(COPY "${BUILD_DIR}/compile_commands.json" DESTINATION "${CMAKE_SOURCE_DIR}")
        emit_compile_flags("${BUILD_DIR}/compile_commands.json")
    endif()

    if(EXISTS "${BUILD_DIR}/env.cmake")
        include("${BUILD_DIR}/env.cmake")
    endif()

    file(RELATIVE_PATH RELATIVE_BUILD_DIR "${WORKSPACE_DIR}" "${BUILD_DIR}")

    if("${TARGETS}" STREQUAL "*")
        set(BUILD_OPTIONS "${RELATIVE_BUILD_DIR}")
    else()
        set(BUILD_OPTIONS "${RELATIVE_BUILD_DIR}" --target ${TARGETS})
    endif()

    list(APPEND BUILD_OPTIONS --config ${BUILD_TYPE})

    if(BUILD_TOOL_OPTIONS)
        list(APPEND BUILD_OPTIONS -- ${BUILD_TOOL_OPTIONS})
    endif()

    string(REGEX REPLACE "[; ]+" " " OPTIONS_STRING "${BUILD_OPTIONS}")
    command_message(CYAN BOLD "${CMAKE_COMMAND} --build ${OPTIONS_STRING}")

    if(CONTEXT_BULK)
        set(${CONTEXT_BULK} ${${CONTEXT_BULK}} COMMAND ${CMAKE_COMMAND} --build ${BUILD_OPTIONS} PARENT_SCOPE)
        return()
    endif()

    if(NOT SHOW_COMMAND_OUTPUT)
        set(OUTPUT_OPTIONS OUTPUT_VARIABLE OUTPUT)
    endif()

    execute_process(
        COMMAND ${CMAKE_COMMAND} --build ${BUILD_OPTIONS}
        WORKING_DIRECTORY "${WORKSPACE_DIR}"
        RESULT_VARIABLE RESULT ${OUTPUT_OPTIONS}
    )

    if(NOT ${RESULT} EQUAL 0)
        if(NOT SHOW_COMMAND_OUTPUT)
            file(WRITE "${BUILD_DIR}/build.log" "${OUTPUT}")
            if("${TARGET}" STREQUAL "*")
                command_error("Failed to build packages, see ${BUILD_DIR}/build.log(error: ${RESULT})")
            else()
                command_error("Failed to build package \"${TARGET}\", see ${BUILD_DIR}/build.log(error: ${RESULT})")
            endif()
        else()
            command_error("Error: ${RESULT}")
        endif()
    endif()
endfunction()

function(execute)
    args_separate(
        BUILD_OPTIONS BUILD_TOOL_OPTIONS
        ARGS ${ARGN}
    )

    args_parse(
        POSITIONAL
            TARGETS...
        OPTIONS
            "+ PROFILES     -p|--profile|PROFILE                    = *"
            "+ BUILD_TYPES  -t|--build_type|--build-type|BUILD_TYPE = debug"
            "? MULTI        -m|--multi|MULTI"
            "? PACKAGES     --packages|PACKAGES"
            "? RECURSE      --recurse|RECURSE"
            "? CONFIGURE    -c|--configure|CONFIGURE"
        ARGS ${BUILD_OPTIONS}
    )

    if ("${PROFILES}" STREQUAL "*")
        set(PROFILES "${DEFAULT_CONAN_PROFILE}")
    endif()

    if("${BUILD_TYPES}" STREQUAL "*" OR "${BUILD_TYPES}" STREQUAL "all")
        set(BUILD_TYPES Debug Release)
    else()
        string(REGEX REPLACE "( *,|\\+ *)" ";" BUILD_TYPES "${BUILD_TYPES}")
    endif()

    if(CONFIGURE)
        set(CONFIGS)

        foreach(PROFILE IN LISTS PROFILES)
            foreach(BUILD_TYPE IN LISTS BUILD_TYPES)
                assume_build_settings(BUILD_DIR BUILD_TYPE "${WORKSPACE_DIR}" "${PROFILE}" "${BUILD_TYPE}" ${MULTI})

                if(NOT EXISTS "${BUILD_DIR}")
                    list(APPEND CONFIGS "${PROFILE}/${BUILD_TYPE}")
                endif()
            endforeach()
        endforeach()

        if(CONFIGS)
            string(REPLACE ";" ", " CONFIGS "${CONFIGS}")
            command_info("Missing configurations: ${CONFIGS}")
            configure(CONFIGS "${CONFIGS}")
        endif()
    endif()

    set(BUILD_COMMANDS)

    foreach(PROFILE IN LISTS PROFILES)
        foreach(BUILD_TYPE IN LISTS BUILD_TYPES)
            assume_build_settings(BUILD_DIR BUILD_TYPE "${WORKSPACE_DIR}" "${PROFILE}" "${BUILD_TYPE}" ${MULTI})

            if(NOT EXISTS "${BUILD_DIR}")
                command_error("There is no build directory for the specified settings, run `asd configure -t=${BUILD_TYPE} -p=${PROFILE}`")
            endif()

            if(PACKAGES)
                set(BUILD_OPTIONS PACKAGES ${TARGETS})

                if(RECURSE)
                    list(APPEND BUILD_OPTIONS RECURSE)
                endif()
            else()
                set(BUILD_OPTIONS TARGETS ${TARGETS})

                if(RECURSE)
                    message(WARNING "Ignore --recurse option: --packages option was not specified")
                endif()
            endif()

            build("${BUILD_DIR}" "${BUILD_TYPE}" BULK BUILD_COMMANDS ${BUILD_OPTIONS})
        endforeach()
    endforeach()

    if(NOT SHOW_COMMAND_OUTPUT)
        set(OUTPUT_OPTIONS OUTPUT_VARIABLE OUTPUT)
    endif()

    execute_process(
        ${BUILD_COMMANDS}
        WORKING_DIRECTORY "${WORKSPACE_DIR}"
        RESULT_VARIABLE RESULT ${OUTPUT_OPTIONS}
    )

    if(NOT ${RESULT} EQUAL 0)
        if(NOT SHOW_COMMAND_OUTPUT)
            file(WRITE "${BUILD_DIR}/build.log" "${OUTPUT}")
            if("${TARGET}" STREQUAL "*")
                command_error("Failed to build packages, see ${BUILD_DIR}/build.log(error: ${RESULT})")
            else()
                command_error("Failed to build package \"${TARGET}\", see ${BUILD_DIR}/build.log(error: ${RESULT})")
            endif()
        else()
            command_error("Error: ${RESULT}")
        endif()
    endif()

    command_success("Done!")
endfunction()
