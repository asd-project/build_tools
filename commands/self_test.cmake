
cmake_minimum_required(VERSION 3.19)

#------------------------------------------------------------------------------

set(PLAYGROUND_DIR "${BUILD_TOOLS}/.tmp")
set(CMAKE_BUILD_TYPE Release)
set(SHOW_COMMAND_OUTPUT OFF)

#------------------------------------------------------------------------------

include(commands/common)
include(utils/args)

#------------------------------------------------------------------------------

function(assert)
    set(ARGUMENTS)
    set(INDEX 0)

    while(${INDEX} LESS ${ARGC})
        if("${ARGV${INDEX}}" MATCHES ".*;.*" OR "${ARGV${INDEX}}" STREQUAL "")
            set(ARGUMENTS "${ARGUMENTS}\"${ARGV${INDEX}}\" ")
        else()
            set(ARGUMENTS "${ARGUMENTS}${ARGV${INDEX}} ")
        endif()

        math(EXPR INDEX "${INDEX} + 1")
    endwhile()

    string(REPLACE ";" "\\\;" ARGUMENTS_STRING "${ARGUMENTS}")

    cmake_language(EVAL CODE "
        if(NOT(${ARGUMENTS}))
            command_error([[Assertion Failed: ${ARGUMENTS_STRING}]])
        endif()"
    )

    if(VERBOSE)
        message("Assertion passed: ${ARGUMENTS}")
    endif()

    math(EXPR ASSERTIONS_PASSED "${ASSERTIONS_PASSED} + 1")
    set(ASSERTIONS_PASSED ${ASSERTIONS_PASSED} CACHE INTERNAL "" FORCE)
endfunction()

set(BASE_PACKAGES "${BUILD_TOOLS}")

foreach(package meta container hopscotch-map ctti launch)
    if(EXISTS "${ROOT}/${package}")
        set(BASE_PACKAGES ${BASE_PACKAGES} "${ROOT}/${package}")
    endif()
endforeach()

function(testcase function message)
    file(REMOVE_RECURSE "${PLAYGROUND_DIR}")
    file(REMOVE_RECURSE "${CMAKE_BINARY_DIR}")

    file(MAKE_DIRECTORY "${PLAYGROUND_DIR}")
    file(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}")

    set(WORKSPACE_DIR "${PLAYGROUND_DIR}")

    message("================================================================================")
    message("Load test case \"${message}\"...")

    set(ASSERTIONS_PASSED 0 CACHE INTERNAL "" FORCE)

    cmake_language(CALL "${function}")

    if(NOT NO_CLEANUP)
        file(REMOVE_RECURSE "${PLAYGROUND_DIR}")
        file(REMOVE_RECURSE "${CMAKE_BINARY_DIR}")
    endif()

    message("${ASSERTIONS_PASSED} assertion(s) passed")
    unset(ASSERTIONS_PASSED CACHE)
endfunction()

macro(test_configure_base)
    if(NOT VERBOSE)
        set(OUTPUT_OPTIONS OUTPUT_VARIABLE OUTPUT ERROR_VARIABLE OUTPUT)
    endif()

    execute_process(COMMAND ${CMAKE_COMMAND} "${PLAYGROUND_DIR}"
        -DCMAKE_BUILD_TYPE=Release
        -DCMAKE_C_COMPILER_WORKS=TRUE
        -DCMAKE_CXX_COMPILER_WORKS=TRUE
        -DCMAKE_C_COMPILER_FORCED=TRUE
        -DCMAKE_CXX_COMPILER_FORCED=TRUE
        "-DWORKSPACE_DIR=${CMAKE_BINARY_DIR}"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
        RESULT_VARIABLE RESULT
        ${OUTPUT_OPTIONS}
        ENCODING UTF8
    )

    if(NOT "${RESULT}" EQUAL 0)
        if(NOT VERBOSE)
            file(WRITE "${PLAYGROUND_DIR}/test.log" "${OUTPUT}")
            command_error("Failed to configure test project, see ${PLAYGROUND_DIR}/test.log")
        else()
            command_error("Failed to configure test project")
        endif()
    endif()
endmacro()

macro(test_configure_packages)
    set(PACKAGES ${BASE_PACKAGES} ${ARGN})

    string(REPLACE ";" "\n    " PACKAGES_STRING "\n    ${ARGN}")
    message("Configure packages:${PACKAGES_STRING}...")

    configure_file("${BUILD_TOOLS}/templates/test_workspace.txt" "${PLAYGROUND_DIR}/CMakeLists.txt" @ONLY)

    test_configure_base()
endmacro()

macro(test_configure_from_text TEXT)
    file(WRITE "${PLAYGROUND_DIR}/CMakeLists.txt" "${TEXT}")

    test_configure_base()
endmacro()

#------------------------------------------------------------------------------

function(execute)
    set(CMAKE_BINARY_DIR "${WORKSPACE_DIR}/build/.tmp")

    args_parse(
        POSITIONAL
            TEST_NAMES...
        OPTIONS
            "? NO_CLEANUP   --no-cleanup|NO_CLEANUP"
            "? VERBOSE      --verbose|VERBOSE"
        ARGS ${ARGN}
    )

    if(VERBOSE)
        set(SHOW_COMMAND_OUTPUT ON)
    endif()

    if(TEST_NAMES)
        set(TESTS)

        foreach(TEST_NAME IN LISTS TEST_NAMES)
            list(APPEND TESTS "${BUILD_TOOLS}/tests/test-${TEST_NAME}.cmake")
        endforeach()
    else()
        file(GLOB TESTS "${BUILD_TOOLS}/tests/test-*.cmake")
    endif()

    foreach(TEST ${TESTS})
        include("${TEST}")
    endforeach()
endfunction()
