import os

from conans import ConanFile, tools

class BuildToolsConan(ConanFile):
    name = "asd.build_tools"
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/build-tools"
    description = "asd build scripts"
    topics = ("asd", "build")
    generators = "cmake", "virtualenv"
    settings = "os", "compiler", "arch", "build_type"
    exports_sources = "templates*", "*.cmake"
    no_copy_source = True
    options = {
        "rtti": [True, False],
        "exceptions": [True, False]
    }
    default_options = {
        "rtti": False,
        "exceptions": True
    }

    def source(self):
        pass

    def package(self):
        self.copy("*.cmake", dst=".", src=".")
        self.copy("*", dst="templates", src="templates")

    def _is_wsl(self):
        if tools.os_info.detect_windows_subsystem() == 'WSL':
            return True

        if tools.os_info.is_linux:
            os_release = tools.load('/proc/sys/kernel/osrelease')
            return 'microsoft' in os_release.casefold()

        return False

    def platform_flags(self):
        return {
            'WSL': self._is_wsl(),
            'EMSCRIPTEN': self.settings.os == 'Emscripten',
        }

    def package_info(self):
        common_flags = []
        cflags = []
        cxxflags = []

        if self.settings.compiler in ['clang', 'gcc', 'apple-clang']:
            common_flags.extend(['-Wall', '-fvisibility=hidden'])
            cxxflags.extend(['-std=c++2a'])
        elif self.settings.compiler == 'Visual Studio':
            cxxflags.extend(['/std:c++latest', '/permissive-', '/bigobj', '-D_UNICODE', '-DUNICODE', '-DNOMINMAX', '-D_HAS_DEPRECATED_RESULT_OF', '-D_WIN32_WINNT=0x0601'])

        if self.settings.compiler == 'apple-clang':
            cxxflags.extend(['-isystem', '/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/v1/'])

        if self.settings.compiler == 'gcc':
            cxxflags.extend(['-no-pie'])

        if not self.options.rtti:
            if self.settings.compiler == 'Visual Studio':
                cxxflags.extend(['/GR-'])
            else:
                cxxflags.extend(['-fno-rtti'])

            common_flags.extend(['-DASD_OPTION_RTTI=0'])
        else:
            common_flags.extend(['-DASD_OPTION_RTTI=1'])

        if not self.options.exceptions:
            if self.settings.compiler == 'Visual Studio':
                cxxflags.extend(['/EHs-c-', '/D_HAS_EXCEPTIONS=0'])
            else:
                cxxflags.extend(['-fno-exceptions'])

            common_flags.extend(['-DASD_OPTION_EXCEPTION=0'])
        else:
            if self.settings.compiler == 'Visual Studio':
                cxxflags.extend(['/EHsc'])

            common_flags.extend(['-DASD_OPTION_EXCEPTION=1'])

        platform_flags = self.platform_flags()

        for (name, value) in platform_flags.items():
            common_flags.extend(['-DASD_PLAT_%s=%d' % (name, value if 1 else 0)])

        self.env_info.PATH.append(self.package_folder)
        self.user_info.module_path = self.package_folder.replace(os.sep, '/')
        self.cpp_info.includedirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.resdirs = []
        self.cpp_info.bindirs = []
        self.cpp_info.cflags = cflags + common_flags
        self.cpp_info.cxxflags = cxxflags + common_flags
