
include(setup)

include(utils/locate_package)
include(utils/message)

include(conan/attributes)

if("${CONAN_PROFILE}" STREQUAL "")
    set(CONAN_PROFILE "${DEFAULT_CONAN_PROFILE}")
endif()

set(CONAN_PROFILE ${CONAN_PROFILE} CACHE STRING "Conan profile" FORCE)

function(register_project_files OUTPUT)
    set(FILES "${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt")

    if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/conanfile.py")
        list(APPEND FILES "${CMAKE_CURRENT_SOURCE_DIR}/conanfile.py")
    elseif(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/conanfile.txt")
        list(APPEND FILES "${CMAKE_CURRENT_SOURCE_DIR}/conanfile.txt")
    endif()

    set(${OUTPUT} ${FILES} PARENT_SCOPE)
endfunction()

function(remap_target TARGET_NAME)
    set_target_properties(${TARGET_NAME}
        PROPERTIES
        MAP_IMPORTED_CONFIG_MINSIZEREL Release
        MAP_IMPORTED_CONFIG_RELWITHDEBINFO Release
    )

    get_target_property(CONAN_IMPORT_TARGETS "${TARGET_NAME}" INTERFACE_LINK_LIBRARIES)
    list(FILTER CONAN_IMPORT_TARGETS INCLUDE REGEX CONAN_PKG::.+)

    foreach(IMPORTED_TARGET ${CONAN_IMPORT_TARGETS})
        if(NOT ";${REMAPPED_TARGETS};" MATCHES ";${IMPORTED_TARGET};")
            set(REMAPPED_TARGETS ${REMAPPED_TARGETS} ${IMPORTED_TARGET} CACHE INTERNAL "remapped")
            remap_target(${IMPORTED_TARGET})
        endif()
    endforeach()
endfunction()

function(import_predefined_dependencies MODULE_NAME TARGET_NAME)
    if(CONAN_PACKAGE_NAME AND NOT "${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "APPLICATION")
        set(CONAN_TARGET_NAME "CONAN_PKG::${CONAN_PACKAGE_NAME}")
        set(PACKAGE_TARGETS_${CONAN_TARGET_NAME} ${PACKAGE_TARGETS_${CONAN_TARGET_NAME}} "${TARGET_NAME}" CACHE INTERNAL "" FORCE)
    endif()

    if("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "INLINE")
        set(TARGET_SCOPE INTERFACE)
    else()
        set(TARGET_SCOPE PUBLIC)

        foreach(CONAN_TARGET ${CONAN_TARGETS})
            target_link_libraries("${TARGET_NAME}" ${TARGET_SCOPE} ${CONAN_TARGET})

            if(PACKAGE_TARGETS_${CONAN_TARGET})
                foreach(PACKAGE_TARGET ${PACKAGE_TARGETS_${CONAN_TARGET}})
                    target_link_libraries("${TARGET_NAME}" PRIVATE ${PACKAGE_TARGET})
                endforeach()
            endif()
        endforeach()
    endif()

    set(REMAPPED_TARGETS "" CACHE INTERNAL "remapped")
    remap_target("${TARGET_NAME}")
    unset(REMAPPED_TARGETS CACHE)

    get_target_property(target_type "${TARGET_NAME}" TYPE)

    # import shared libraries
    if(target_type STREQUAL "EXECUTABLE")
        # color_message(WHITE "   Collect shared libraries...")

        set(OUTPUT_FILES)
        set(SHARED_LIBS_DEPENDENCIES)

        foreach(dep ${CONAN_DEPENDENCIES})
            string(TOUPPER ${dep} DEP)
            set(DEPENDENCY_SHARED_LIBS)

            foreach(libdir ${CONAN_LIB_DIRS_${DEP}})
                file(GLOB shared_libs LIST_DIRECTORIES false RELATIVE "${libdir}/.." "${libdir}/*.so" "${libdir}/*.dll" "${libdir}/*.dylib")

                foreach(shared_lib ${shared_libs})
                    set(INPUT_FILE "${libdir}/../${shared_lib}")
                    set(OUTPUT_FILE "${${MODULE_NAME}_BINARY_OUTPUT}/${shared_lib}")

                    add_custom_command(
                        OUTPUT "${OUTPUT_FILE}" DEPENDS "${INPUT_FILE}"
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${INPUT_FILE}" "${OUTPUT_FILE}"
                        VERBATIM
                    )

                    list(APPEND DEPENDENCY_SHARED_LIBS "${OUTPUT_FILE}")
                endforeach()
            endforeach()

            if(DEPENDENCY_SHARED_LIBS)
                set(SHARED_LIBS_DEPENDENCIES ${SHARED_LIBS_DEPENDENCIES} "CONAN_PKG::${dep}")
                set(OUTPUT_FILES ${OUTPUT_FILES} ${DEPENDENCY_SHARED_LIBS})
            endif()
        endforeach()

        if(SHARED_LIBS_DEPENDENCIES)
            if(EMSCRIPTEN)
                module_link_files("${MODULE_NAME}_shared_libs" ${OUTPUT_FILES})
            endif()

            add_custom_target("${MODULE_NAME}_shared_libs" DEPENDS ${OUTPUT_FILES})
            add_dependencies("${MODULE_NAME}_shared_libs" ${SHARED_LIBS_DEPENDENCIES})

            set_target_properties("${MODULE_NAME}_shared_libs" PROPERTIES FOLDER "tasks")
            module_custom_dependencies("${MODULE_NAME}_shared_libs")
        endif()
    endif()
endfunction()

macro(conan_setup)
    set(CONAN_CMAKE_SILENT_OUTPUT ON)
    set(CONAN_SYSTEM_INCLUDES ON)
    set(CONAN_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}")

    if(REGENERATE_PACKAGES)
        get_package_attributes(package "${CMAKE_CURRENT_SOURCE_DIR}" "name" "version" "type")
        set(name "${package_name}")
        set(version "${package_version}")
        resolve_package_type(type "${package_type}")

        set(PACKAGE_PATH_${name} "${path}" CACHE PATH "${name} local path" FORCE)
        set(PACKAGE_KEY_${name} "${name}/${version}@${CONAN_USER_CHANNEL}" CACHE PATH "${name} package key" FORCE)

        if("${type}" STREQUAL "multi-config" AND "${CMAKE_BINARY_DIR}" MATCHES "${CMAKE_BUILD_TYPE}$")
            get_filename_component(CONAN_OUTPUT_DIR "${CMAKE_BINARY_DIR}/../${CONAN_PROFILE}/Multi/${name}" ABSOLUTE)
        else()
            file(RELATIVE_PATH PACKAGE_BINARY_PATH "${CMAKE_BINARY_DIR}" "${CMAKE_CURRENT_BINARY_DIR}")
            set(CONAN_OUTPUT_DIR "${REGENERATE_ROOT}/${PACKAGE_BINARY_PATH}")
        endif()

        if(NOT EXISTS "${CONAN_OUTPUT_DIR}")
            message(FATAL_ERROR "Couldn't find package output directory (${CONAN_OUTPUT_DIR}), try to call `asd configure -t=${CMAKE_BUILD_TYPE} -p=${CONAN_PROFILE}` in a package workspace")
        endif()
    endif()

    if(CONAN_IN_LOCAL_CACHE)
        include("${CONAN_OUTPUT_DIR}/conanbuildinfo.cmake")

        if(NOT REGENERATE_PACKAGES)
            conan_basic_setup(TARGETS)
        endif()
    else()
        if("${CMAKE_BUILD_TYPE}" STREQUAL "" AND EXISTS "${CONAN_OUTPUT_DIR}/conanbuildinfo_multi.cmake")
            include("${CONAN_OUTPUT_DIR}/conanbuildinfo_multi.cmake")
        else()
            include("${CONAN_OUTPUT_DIR}/conanbuildinfo.cmake")
        endif()

        if(NOT REGENERATE_PACKAGES)
            conan_basic_setup(TARGETS NO_OUTPUT_DIRS)
        endif()
    endif()

    if(CONAN_PACKAGE_NAME)
        set(CONAN_TARGET_NAME "CONAN_PKG::${CONAN_PACKAGE_NAME}")
        unset(PACKAGE_TARGETS_${CONAN_TARGET_NAME} CACHE)
    endif()
endmacro()

conan_setup()

register_path("${CMAKE_CURRENT_SOURCE_DIR}")
