#--------------------------------------------------------
#   ASD package location utils
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

include(conan/attributes)

function(locate_package package output_path)
    if(NOT "${PACKAGE_PATH_${package}}" STREQUAL "")
        set(${output_path} "${PACKAGE_PATH_${package}}" PARENT_SCOPE)
        set(PACKAGE_FOUND_${package} TRUE PARENT_SCOPE)
        return()
    endif()

    string(TOUPPER "${package}" PACKAGE)

    if (CONAN_${PACKAGE}_ROOT)
        set(package_folder "${CONAN_${PACKAGE}_ROOT}")
    else()
        execute_process(COMMAND conan info --package-filter ${package}/* --paths -n package_folder . OUTPUT_VARIABLE COMMAND_OUTPUT ERROR_VARIABLE COMMAND_OUTPUT RESULT_VARIABLE RESULT WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")

        if(NOT ${RESULT} EQUAL 0)
            set(PACKAGE_FOUND_${package} FALSE PARENT_SCOPE)

            if(NOT ${ARGC} EQUAL 2 AND "${ARGV2}" STREQUAL "OPTIONAL")
                return()
            endif()

            message(FATAL_ERROR "Couldn't locate package: ${package} - ${COMMAND_OUTPUT}")
        endif()

        string(REGEX MATCH "package_folder:[ ]+(.*)$" _ "${COMMAND_OUTPUT}")
        string(REGEX MATCH "^[ \t\n\r]*([^\t\n\r]+[^ \t\n\r])" _ "${CMAKE_MATCH_1}")
        file(TO_CMAKE_PATH "${CMAKE_MATCH_1}" package_folder)

        if(NOT EXISTS "${package_folder}")
            message(FATAL_ERROR "Couldn't locate package: ${package} - ${package_folder} doesn't exist")
        endif()
    endif()

    set(PACKAGE_PATH_${package} "${package_folder}" CACHE PATH "${package} local path" FORCE)
    set(${output_path} "${package_folder}" PARENT_SCOPE)
    set(PACKAGE_FOUND_${package} TRUE PARENT_SCOPE)
endfunction()

macro(register_path path)
    if(NOT "${path}" IN_LIST CMAKE_MODULE_PATH)
        set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${path}" CACHE INTERNAL "CMake module search path")
    endif()
endmacro()

macro(register_package_path package)
    locate_package(${package} package_path ${ARGN})

    if(NOT PACKAGE_FOUND_${package})
        return()
    endif()

    register_path("${package_path}")
endmacro()

macro(package_include package path)
    register_package_path(${package} ${ARGN})

    if(NOT PACKAGE_FOUND_${package})
        message(FATAL_ERROR "Couldn't find package path ${package}")
    endif()

    include(${path})
endmacro()
