#--------------------------------------------------------
#   ASD message output utils
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

if(${FORCE_COLORIZE})
    set(USE_COLORS ON)
else()
    set(USE_COLORS OFF)
endif()

unset(FORCE_COLORIZE CACHE)

set(MESSAGE_ENTER_STATUS " >")
set(MESSAGE_UPDATE_STATUS "::")
set(MESSAGE_ADD_STATUS " +")
set(MESSAGE_REMOVE_STATUS " -")
set(MESSAGE_WARNING_STATUS " !")
set(MESSAGE_SUCCESS_STATUS " ✓")

set(ESCAPE_CODE_RESET "")
set(ESCAPE_CODE_NORMAL 0)
set(ESCAPE_CODE_BOLD 1)
set(ESCAPE_CODE_UNDERLINE 4)
set(ESCAPE_CODE_BLINK 5)
set(ESCAPE_CODE_INVERSE 7)
set(ESCAPE_CODE_BLACK 30)
set(ESCAPE_CODE_RED 31)
set(ESCAPE_CODE_GREEN 32)
set(ESCAPE_CODE_YELLOW 33)
set(ESCAPE_CODE_BLUE 34)
set(ESCAPE_CODE_MAGENTA 35)
set(ESCAPE_CODE_CYAN 36)
set(ESCAPE_CODE_WHITE 37)

string(ASCII 27 ESCAPE_CHARACTER)

function(append_escape_sequence OUTPUT MESSAGE_BUFFER CODE)
    if(USE_COLORS)
        set(${OUTPUT} "${MESSAGE_BUFFER}${ESCAPE_CHARACTER}[${CODE}m" PARENT_SCOPE)
    endif()
endfunction()

function(color_message)
    set(MESSAGE_BUFFER)

    foreach(FRAGMENT ${ARGN})
        if(DEFINED "ESCAPE_CODE_${FRAGMENT}")
            append_escape_sequence(MESSAGE_BUFFER "${MESSAGE_BUFFER}" "${ESCAPE_CODE_${FRAGMENT}}")
        else()
            set(MESSAGE_BUFFER "${MESSAGE_BUFFER}${FRAGMENT}")
        endif()
    endforeach()

    append_escape_sequence(MESSAGE_BUFFER "${MESSAGE_BUFFER}" "${ESCAPE_CODE_RESET}")
    message("\r${MESSAGE_BUFFER}") # \r fixes interleaved output on Windows
endfunction()

function(remove_output_formatting OUTPUT TEXT)
    string(REGEX REPLACE "${ESCAPE_CHARACTER}\\[[^m]*m" "" TEXT "${TEXT}")
    set(${OUTPUT} "${TEXT}" PARENT_SCOPE)
endfunction()
