#--------------------------------------------------------
#   ASD file configuration utility
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

include(utils/message)
include(utils/text)

#--------------------------------------------------------

set(FILE_CFG_SOURCE_TEMPLATES default)
set(FILE_CFG_SOURCE_TEMPLATE_FILES_default "${BUILD_TOOLS}/templates")

function(file_cfg_register_templates key)
    if(NOT "${key}" IN_LIST FILE_CFG_SOURCE_TEMPLATES)
        set(FILE_CFG_SOURCE_TEMPLATES ${FILE_CFG_SOURCE_TEMPLATES} "${key}" PARENT_SCOPE)
        set(FILE_CFG_SOURCE_TEMPLATE_FILES_${key} ${ARGN} PARENT_SCOPE)
    endif()
endfunction()

function(file_cfg FILE_PATH)
    get_filename_component(DIRECTORY_PATH "${FILE_PATH}" DIRECTORY)
    get_filename_component(FILE_EXTENSION "${FILE_PATH}" EXT)
    get_filename_component(FILE_NAME "${FILE_PATH}" NAME_WE)

    if(FILE_CFG_SOURCE_ROOT)
        file(RELATIVE_PATH DOMAIN_DIRECTORY_PATH "${FILE_CFG_SOURCE_ROOT}" "${DIRECTORY_PATH}")
        set(short_path "${DOMAIN_DIRECTORY_PATH}/${FILE_NAME}")
    else()
        set(short_path "${FILE_NAME}")
    endif()

    if(NOT EXISTS "${DIRECTORY_PATH}")
        file(MAKE_DIRECTORY "${DIRECTORY_PATH}")
    endif()

    name_upper(FILE_NAME_UPPER "${FILE_NAME}")
    name_lower(FILE_NAME_LOWER "${FILE_NAME}")

    if (FILE_CFG_NAMESPACE)
        set(namespace "${FILE_CFG_NAMESPACE}")
    elseif(FILE_CFG_SOURCE_ROOT)
        file(RELATIVE_PATH RELATIVE_NAMESPACE_PATH "${FILE_CFG_SOURCE_ROOT}" "${DIRECTORY_PATH}")
        string(REGEX REPLACE "/" "::" namespace "${RELATIVE_NAMESPACE_PATH}")
    else()
        set(namespace "NAMESPACE")
    endif()

    file(RELATIVE_PATH RELATIVE_SRC_PATH "${WORKSPACE_DIR}" "${FILE_PATH}")
    set(MESSAGE GREEN BOLD "${MESSAGE_ADD_STATUS} " RESET "Create new file at " WHITE BOLD "${RELATIVE_SRC_PATH}")

    if(FILE_CFG_TEMPLATE)
        set(REGISTERED_PATHS)

        foreach(template ${FILE_CFG_SOURCE_TEMPLATES})
            foreach(template_path ${FILE_CFG_SOURCE_TEMPLATE_FILES_${template}})
                list(APPEND REGISTERED_PATHS "${template_path}")
                set(template_file "${template_path}/${FILE_CFG_TEMPLATE}")

                if(EXISTS "${template_file}")
                    file(RELATIVE_PATH RELATIVE_TEMPLATE_PATH "${WORKSPACE_DIR}" "${template_file}")
                    color_message(${MESSAGE} RESET " from template ${RELATIVE_TEMPLATE_PATH}")

                    configure_file("${template_file}" "${FILE_PATH}")
                    return()
                endif()
            endforeach()
        endforeach()

        message(FATAL_ERROR "Couldn't find template ${FILE_CFG_TEMPLATE} in registered paths: ${REGISTERED_PATHS}")
    endif()

    file(RELATIVE_PATH DIR_NAME "${CMAKE_CURRENT_SOURCE_DIR}" "${DIRECTORY_PATH}")
    set(variants "${DIR_NAME}/${FILE_NAME}${FILE_EXTENSION}" "${DIR_NAME}/_${FILE_EXTENSION}" "${FILE_NAME}${FILE_EXTENSION}" "_${FILE_EXTENSION}")

    foreach(template ${FILE_CFG_SOURCE_TEMPLATES})
        foreach(template_path ${FILE_CFG_SOURCE_TEMPLATE_FILES_${template}})
            foreach(variant ${variants})
                set(template_file "${template_path}/${variant}")

                if(EXISTS "${template_file}")
                    file(RELATIVE_PATH RELATIVE_TEMPLATE_PATH "${WORKSPACE_DIR}" "${template_file}")
                    color_message(${MESSAGE} RESET " from template ${RELATIVE_TEMPLATE_PATH}")

                    configure_file("${template_file}" "${FILE_PATH}")
                    return()
                endif()
            endforeach()
        endforeach()
    endforeach()

    color_message(${MESSAGE} RESET " (empty)")
    file(WRITE "${FILE_PATH}" "")
endfunction()
