#--------------------------------------------------------
#   ASD package manifest utils
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

function(manifest_read MANIFEST_PATH ROOT_PREFIX)
    file(READ "${MANIFEST_PATH}" MANIFEST_CONTENTS)
    string(JSON PROPERTIES_COUNT LENGTH "${MANIFEST_CONTENTS}")

    set(PROPERTIES)

    # Index loops in cmake are inclusive on both ends(weird), so we need to calculate actual upper bound
    math(EXPR LAST_INDEX "${PROPERTIES_COUNT} - 1")
    foreach(INDEX RANGE ${LAST_INDEX})
        string(JSON KEY MEMBER "${MANIFEST_CONTENTS}" ${INDEX})
        string(JSON VALUE GET "${MANIFEST_CONTENTS}" "${KEY}")

        list(APPEND PROPERTIES "${KEY}")
        set(${ROOT_PREFIX}_${KEY} "${VALUE}" PARENT_SCOPE)
    endforeach()

    set(${ROOT_PREFIX}_PROPERTIES ${PROPERTIES} PARENT_SCOPE)
    set(${ROOT_PREFIX}_JSON "${MANIFEST_CONTENTS}" PARENT_SCOPE)
endfunction()

function(manifest_write MANIFEST_PATH ROOT_PREFIX)
    set(MANIFEST_CONTENTS "${${ROOT_PREFIX}_JSON}")

    if(${ROOT_PREFIX}_CHANGED)
        string(REGEX REPLACE " :" ":" MANIFEST_CONTENTS "${MANIFEST_CONTENTS}")
        string(REGEX REPLACE "\n( +)\"" "\n\\1\\1\"" MANIFEST_CONTENTS "${MANIFEST_CONTENTS}")
        set(MANIFEST_CONTENTS "${MANIFEST_CONTENTS}\n")

        set(${ROOT_PREFIX}_JSON "${MANIFEST_CONTENTS}" PARENT_SCOPE)
        set(${ROOT_PREFIX}_CHANGED FALSE PARENT_SCOPE)
    endif()

    file(WRITE "${MANIFEST_PATH}" "${MANIFEST_CONTENTS}")
endfunction()

function(manifest_set ROOT_PREFIX KEY VALUE)
    set(PROPERTIES ${ROOT_PREFIX}_PROPERTIES)

    if(NOT "${${ROOT_PREFIX}_JSON}" STREQUAL "")
        set(MANIFEST_CONTENTS "${${ROOT_PREFIX}_JSON}")

        if(NOT "${KEY}" IN_LIST PROPERTIES)
            list(APPEND PROPERTIES "${KEY}")
        endif()
    else()
        set(MANIFEST_CONTENTS "{}\n")
        list(APPEND PROPERTIES "${KEY}")
    endif()

    string(JSON MANIFEST_CONTENTS SET "${MANIFEST_CONTENTS}" "${KEY}" "${VALUE}")

    set(${ROOT_PREFIX}_${KEY} "${VALUE}" PARENT_SCOPE)
    set(${ROOT_PREFIX}_PROPERTIES ${PROPERTIES} PARENT_SCOPE)
    set(${ROOT_PREFIX}_JSON "${MANIFEST_CONTENTS}" PARENT_SCOPE)
    set(${ROOT_PREFIX}_CHANGED TRUE PARENT_SCOPE)
endfunction()

function(manifest_remove ROOT_PREFIX KEY)
    if("${${ROOT_PREFIX}_JSON}" STREQUAL "")
        set(${ROOT_PREFIX}_JSON "{}\n" PARENT_SCOPE)
        return()
    endif()

    set(MANIFEST_CONTENTS "${${ROOT_PREFIX}_JSON}")
    set(PROPERTIES ${ROOT_PREFIX}_PROPERTIES)

    if(NOT "${KEY}" IN_LIST PROPERTIES)
        return()
    endif()

    list(REMOVE_ITEM PROPERTIES "${KEY}")
    string(JSON MANIFEST_CONTENTS REMOVE "${MANIFEST_CONTENTS}" "${KEY}")

    unset(${ROOT_PREFIX}_${KEY} PARENT_SCOPE)
    set(${ROOT_PREFIX}_PROPERTIES ${PROPERTIES} PARENT_SCOPE)
    set(${ROOT_PREFIX}_JSON "${MANIFEST_CONTENTS}" PARENT_SCOPE)
    set(${ROOT_PREFIX}_CHANGED TRUE PARENT_SCOPE)
endfunction()

macro(manifest_set_parent_scope output ROOT_PREFIX)
    set(${output}_PROPERTIES ${${ROOT_PREFIX}_PROPERTIES} PARENT_SCOPE)
    set(${output}_JSON "${ROOT_PREFIX}_JSON" PARENT_SCOPE)

    foreach(KEY IN LISTS ${ROOT_PREFIX}_PROPERTIES)
        set(${output}_${KEY} "${${ROOT_PREFIX}_${KEY}}" PARENT_SCOPE)
    endforeach()
endmacro()
