#--------------------------------------------------------
#   ASD file utils
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

include(utils/message)

#--------------------------------------------------------

set(SOURCE_TEMPLATES default CACHE INTERNAL "" FORCE)
set(SOURCE_TEMPLATE_FILES_default "${BUILD_TOOLS}/templates" CACHE INTERNAL "" FORCE)

function(register_templates key)
    if(NOT "${key}" IN_LIST SOURCE_TEMPLATES)
        set(SOURCE_TEMPLATES ${SOURCE_TEMPLATES} "${key}" CACHE INTERNAL "" FORCE)
        set(SOURCE_TEMPLATE_FILES_${key} ${ARGN} CACHE INTERNAL "" FORCE)
    endif()
endfunction()

function(create_source SRC_ROOT SRC_PATH)
    set(SRC_FULLPATH "${SRC_ROOT}/${SRC_PATH}")

    get_filename_component(SRC_DIR "${SRC_FULLPATH}" DIRECTORY)
    get_filename_component(SRC_EXT "${SRC_FULLPATH}" EXT)
    get_filename_component(SRC_SHORT_DIR "${SRC_PATH}" DIRECTORY)
    get_filename_component(DIR_NAME "${SRC_SHORT_DIR}" NAME_WE)
    get_filename_component(FILE_NAME "${SRC_FULLPATH}" NAME_WE)

    if(SRC_SHORT_DIR)
        set(short_path "${SRC_SHORT_DIR}/${FILE_NAME}")
    else()
        set(short_path "${FILE_NAME}")
    endif()

    if(NOT EXISTS "${SRC_DIR}")
        file(MAKE_DIRECTORY "${SRC_DIR}")
    endif()

    string(REGEX REPLACE "\\W" "_" PROJECT_PREFIX "${SRC_SHORT_DIR}")

    name_upper(FILE_NAME_UPPER "${FILE_NAME}")
    name_lower(FILE_NAME_LOWER "${FILE_NAME}")
    name_upper(PROJECT_PREFIX_UPPER "${PROJECT_PREFIX}")
    name_lower(PROJECT_PREFIX_LOWER "${PROJECT_PREFIX}")

    string(REGEX REPLACE "/" "::" PROJECT_PREFIX "${PROJECT_PREFIX}")

    if (MODULE_NAMESPACE)
        set(namespace "${MODULE_NAMESPACE}::${PROJECT_PREFIX}")
    else()
        set(namespace "${PROJECT_PREFIX}")
    endif()

    set(variants "${DIR_NAME}/${FILE_NAME}${SRC_EXT}" "${DIR_NAME}/_${SRC_EXT}" "${FILE_NAME}${SRC_EXT}" "_${SRC_EXT}")

    file(RELATIVE_PATH RELATIVE_SRC_PATH "${WORKSPACE_DIR}" "${SRC_FULLPATH}")
    set(MESSAGE GREEN BOLD "${MESSAGE_ADD_STATUS} " RESET "Create new source file " WHITE BOLD "${RELATIVE_SRC_PATH}")

    foreach(variant ${variants})
        foreach(template ${SOURCE_TEMPLATES})
            foreach(template_path ${SOURCE_TEMPLATE_FILES_${template}})
                set(template_file "${template_path}/${variant}")

                if(EXISTS "${template_file}")
                    file(RELATIVE_PATH RELATIVE_TEMPLATE_PATH "${WORKSPACE_DIR}" "${template_file}")
                    color_message(${MESSAGE} RESET " from template ${RELATIVE_TEMPLATE_PATH}")

                    configure_file("${template_file}" "${SRC_FULLPATH}")
                    return()
                endif()
            endforeach()
        endforeach()
    endforeach()

    color_message(${MESSAGE} RESET " (empty)")
    file(WRITE "${SRC_FULLPATH}" "")
endfunction()
