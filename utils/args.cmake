#--------------------------------------------------------
#   ASD argument parsing utils
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

#--------------------------------------------------------

include_guard(GLOBAL)

include(commands/common)

#--------------------------------------------------------

function(args_separate COMMON_ARGS)
    cmake_parse_arguments(CONTEXT "" "" "ARGS" ${ARGN})

    set(SEPARATED_NAMES ${CONTEXT_UNPARSED_ARGUMENTS})
    set(CURRENT_SEPARATOR "--")
    set(SEPARATORS)

    foreach(name IN LISTS SEPARATED_NAMES)
        set(SEPARATOR_NAME_${CURRENT_SEPARATOR} "${name}")

        list(APPEND SEPARATORS "${CURRENT_SEPARATOR}")
        set(CURRENT_SEPARATOR "${CURRENT_SEPARATOR}-")
    endforeach()

    cmake_parse_arguments(SEPARATED "" "" "${SEPARATORS}" ${CONTEXT_ARGS})

    set(${COMMON_ARGS} ${SEPARATED_UNPARSED_ARGUMENTS} PARENT_SCOPE)

    foreach(separator IN LISTS SEPARATORS)
        set(${SEPARATOR_NAME_${separator}} ${SEPARATED_${separator}} PARENT_SCOPE)
    endforeach()
endfunction()

function(_match_option output arg)
    foreach(option IN LISTS OPTIONS)
        set(pattern "${${option}_OPTION_PATTERN}")

        if("${arg}" MATCHES "^(${pattern})$")
            set(${output} ${option} PARENT_SCOPE)
            return()
        endif()
    endforeach()

    set(${output} "" PARENT_SCOPE)
endfunction()

function(args_print_format)
    message(${ARGN}
"Format:
    = <OPTION_NAME> <MATCHES> [= <DEFAULT_VALUE>]      - declare option with value
    + <OPTION_NAME> <MATCHES> [= <DEFAULT_VALUE>]      - declare option with multiple values
    ? <OPTION_NAME> <MATCHES>                          - declare option without value"
    )
endfunction()

function(args_parse)
    cmake_parse_arguments(CONTEXT "" "" "POSITIONAL;OPTIONS;ARGS" ${ARGN})

    if(CONTEXT_UNPARSED_ARGUMENTS)
        command_error("Unexpected parameters: ${CONTEXT_UNPARSED_ARGUMENTS}")
    endif()

    set(OPTION_DESCRIPTIONS ${CONTEXT_OPTIONS})
    set(OPTIONS)

    foreach(entry IN LISTS OPTION_DESCRIPTIONS)
        if("${entry}" MATCHES "=[ \t]*([.a-zA-Z0-9_-]+)[ \t]+(.+)")
            set(option "${CMAKE_MATCH_1}")
            set(description "${CMAKE_MATCH_2}")
            set(${option}_HAS_VALUE TRUE)

            if("${description}" MATCHES "([^=]*[^ \t]+)[ \t]*=[ \t]*([^ \t]+)")
                set(${option}_OPTION_PATTERN "${CMAKE_MATCH_1}")
                set(${option}_OPTION_DEFAULT ${CMAKE_MATCH_2})
            else()
                set(${option}_OPTION_PATTERN "${description}")
            endif()
        elseif("${entry}" MATCHES "\\+[ \t]*([.a-zA-Z0-9_-]+)[ \t]+(.+)")
            set(option "${CMAKE_MATCH_1}")
            set(description "${CMAKE_MATCH_2}")
            set(${option}_HAS_VALUE TRUE)
            set(${option}_HAS_MULTIPLE_VALUES TRUE)

            if("${description}" MATCHES "([^=]*[^ \t]+)[ \t]*=[ \t]*([^ \t]+)")
                set(${option}_OPTION_PATTERN "${CMAKE_MATCH_1}")
                set(${option}_OPTION_DEFAULT ${CMAKE_MATCH_2})
            else()
                set(${option}_OPTION_PATTERN "${description}")
            endif()
        elseif("${entry}" MATCHES "\\?[ \t]*([.a-zA-Z0-9_-]+)[ \t]+(.+)")
            set(option "${CMAKE_MATCH_1}")
            set(${option}_OPTION_PATTERN "${CMAKE_MATCH_2}")
            set(${option} OFF)
        else()
            args_print_format(FATAL_ERROR "Incorrect option description: ${entry}\n")
        endif()

        list(APPEND OPTIONS "${option}")
    endforeach()

    set(CURRENT_OPTION)
    set(POSITIONAL_ARGS)

    set(ARGUMENTS)

    foreach(arg IN LISTS CONTEXT_ARGS)
        if("${arg}" MATCHES "^(-[^=]+)=(.*)")
            list(APPEND ARGUMENTS "${CMAKE_MATCH_1}")
            list(APPEND ARGUMENTS "${CMAKE_MATCH_2}")
        else()
            list(APPEND ARGUMENTS "${arg}")
        endif()
    endforeach()

    foreach(arg IN LISTS ARGUMENTS)
        if(CURRENT_OPTION)
            if(${CURRENT_OPTION}_HAS_MULTIPLE_VALUES)
                set(${CURRENT_OPTION} ${${CURRENT_OPTION}} "${arg}")
            else()
                set(${CURRENT_OPTION} ${arg})
            endif()

            unset(CURRENT_OPTION)
            continue()
        endif()

        _match_option(option ${arg})

        if(option)
            if(${option}_HAS_VALUE)
                set(CURRENT_OPTION ${option})
            else()
                set(${option} ON)
            endif()

            continue()
        endif()

        if("${arg}" MATCHES "^--(.+)" OR "${arg}" MATCHES "^-(.+)")
            command_error("Unknown option: ${arg}")
        endif()

        if(CONTEXT_POSITIONAL)
            list(APPEND POSITIONAL_ARGS ${arg})
        else()
            command_error("Unexpected argument: ${arg}")
        endif()
    endforeach()

    foreach(option IN LISTS OPTIONS)
        if("${${option}}" STREQUAL "")
            if (${option}_OPTION_DEFAULT)
                set(${option} ${${option}_OPTION_DEFAULT})
            endif()
        else()
            if (${option}_HAS_MULTIPLE_VALUES)
                list(REMOVE_DUPLICATES "${option}")
            endif()
        endif()

        set(${option} "${${option}}" PARENT_SCOPE)
    endforeach()

    if(CONTEXT_POSITIONAL)
        foreach(POSITIONAL IN LISTS CONTEXT_POSITIONAL)
            if("${POSITIONAL}" MATCHES "(.*)\\.\\.\\.$")
                set(${CMAKE_MATCH_1} ${POSITIONAL_ARGS} PARENT_SCOPE)
                set(POSITIONAL_ARGS)
            elseif(POSITIONAL_ARGS)
                list(POP_FRONT POSITIONAL_ARGS ARG)
                set(${POSITIONAL} ${ARG} PARENT_SCOPE)
            else()
                command_error("No specified value for positional argument ${POSITIONAL}")
            endif()
        endforeach()

        if(POSITIONAL_ARGS)
            command_error("Unexpected positional arguments: ${POSITIONAL_ARGS}")
        endif()
    endif()
endfunction()
