#--------------------------------------------------------
#   ASD text utils
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

function(name_upper out name)
    string(REGEX REPLACE "([a-z])([A-Z])" "\\1_\\2" NAME "${name}")
    string(REGEX REPLACE "[^A-Za-z0-9_]" "_" NAME "${NAME}")
    string(TOUPPER "${NAME}" NAME)

    set(${out} "${NAME}" PARENT_SCOPE)
endfunction()

function(name_lower out name)
    string(REGEX REPLACE "([a-z])([A-Z])" "\\1_\\2" NAME "${name}")
    string(REGEX REPLACE "[^A-Za-z0-9_]" "_" NAME "${NAME}")
    string(TOLOWER "${NAME}" NAME)

    set(${out} "${NAME}" PARENT_SCOPE)
endfunction()

function(join VALUES GLUE OUTPUT)
    string(REGEX REPLACE "([^\\]|^);" "\\1${GLUE}" _TMP_STR "${VALUES}")
    string(REGEX REPLACE "[\\](.)" "\\1" _TMP_STR "${_TMP_STR}") # fixes escaping
    set(${OUTPUT} "${_TMP_STR}" PARENT_SCOPE)
endfunction()

function(escape_regular OUT_STR STR)
    string(REPLACE "\\" "\\\\" BUF "${STR}")
    string(REPLACE "/" "\\/" BUF "${BUF}")
    string(REPLACE "+" "\\+" BUF "${BUF}")
    string(REPLACE "." "\\." BUF "${BUF}")
    string(REPLACE ":" "\\:" BUF "${BUF}")

    set(${OUT_STR} ${BUF} PARENT_SCOPE)
endfunction()
