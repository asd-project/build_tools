
function(join_path OUT ROOT_PATH SUBPATH)
    if("${SUBPATH}" MATCHES "^/(.*)$")
        set(${OUT} "${CMAKE_MATCH_1}" PARENT_SCOPE)
        return()
    endif()

    string(REGEX REPLACE "^\\./" "" ROOT_PATH "${ROOT_PATH}")
    string(REGEX REPLACE "/$" "" ROOT_PATH "${ROOT_PATH}")

    if("${ROOT_PATH}" STREQUAL "")
        set(${OUT} "${SUBPATH}" PARENT_SCOPE)
    else()
        set(${OUT} "${ROOT_PATH}/${SUBPATH}" PARENT_SCOPE)
    endif()
endfunction()

function(normalize_path PATH)
    string(REPLACE "/./" "/" "${PATH}" "${${PATH}}")
    set("${PATH}" "${${PATH}}" PARENT_SCOPE)
endfunction()
