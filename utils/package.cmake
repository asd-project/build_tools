#--------------------------------------------------------
#   ASD package utils
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

set(PACKAGE_SUBDIRS tests examples benchmarks)

function(collect_subpackages OUTPUT PACKAGE_PATH)
    if(NOT IS_DIRECTORY "${PACKAGE_PATH}")
        return()
    endif()

    set(DIRECTORIES ${${OUTPUT}})
    list(APPEND DIRECTORIES "${PACKAGE_PATH}")

    foreach(subdir ${PACKAGE_SUBDIRS})
        list(APPEND DIRECTORIES "${PACKAGE_PATH}/${subdir}")
        file(GLOB FILES LIST_DIRECTORIES true "${PACKAGE_PATH}/${subdir}/*")

        foreach(FILE ${FILES})
            if(IS_DIRECTORY "${FILE}")
                list(APPEND DIRECTORIES "${FILE}")
            endif()
        endforeach()
    endforeach()

    set(${OUTPUT} ${DIRECTORIES} PARENT_SCOPE)
endfunction()

function(resolve_package_type OUTPUT TYPE)
    if("${TYPE}" STREQUAL "app")
        set(${OUTPUT} "application" PARENT_SCOPE)
    elseif("${TYPE}" STREQUAL "lib")
        set(${OUTPUT} "library" PARENT_SCOPE)
    else()
        set(${OUTPUT} "${TYPE}" PARENT_SCOPE)
    endif()
endfunction()
