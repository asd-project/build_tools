
#--------------------------------------------------------
#   ASD file tree utility
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

include(utils/tree)
include(utils/file_cfg)

#--------------------------------------------------------

set(RESERVED_VARIABLES NAME TYPE PATH COMMANDS)

if(FILE_TREE_TRACE)
    function(tree_debug_step TREE STEP)
        tree_combine_path(key ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
        message("${STEP} ${key} ${ARGN}")
    endfunction()

    function(tree_debug_entry TREE KEY)
        set(address ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
        set(key)
        set(commands)
        set(variables_map)

        foreach(segment IN LISTS address)
            join_path(key "${key}" "${segment}")

            foreach(command IN LISTS ${TREE}_${key}_COMMANDS)
                if (commands)
                    set(commands "${commands} [${command}]")
                else()
                    set(commands "[${command}]")
                endif()
            endforeach()

            foreach(name IN LISTS ${TREE}_${key}_VARIABLES)
                if(NOT "${name}" IN_LIST RESERVED_VARIABLES)
                    list(APPEND variables_map "${name}")
                    set("VARIABLE_${name}" ${VARIABLE_${name}} ${${TREE}_${key}_${name}})
                endif()
            endforeach()
        endforeach()

        list(REMOVE_DUPLICATES "variables_map")

        set(variables)

        foreach(name IN LISTS variables_map)
            if (variables)
                set(variables "${variables}, ${name}=\"${VARIABLE_${name}}\"")
            else()
                set(variables "${name}=\"${VARIABLE_${name}}\"")
            endif()
        endforeach()

        set(type "${${TREE}_${KEY}_TYPE}")
        set(path "${${TREE}_${KEY}_PATH}")

        string(SUBSTRING "${type}" 0 3 SHORT_TYPE)
        message("${SHORT_TYPE} ${key} ${path} ${commands} (${variables})")
    endfunction()
endif()

#--------------------------------------------------------

set(FILE_TREE_NODE_TYPE_FILE "LEAF")
set(FILE_TREE_NODE_TYPE_DIRECTORY "PARENT")

set(FILE_TREE_FILTER_ALIAS_FILE TYPE FILE)
set(FILE_TREE_FILTER_ALIAS_DIRECTORY TYPE DIRECTORY)

function(file_tree_filter_paths OUTPUT TREE KEY)
    set(type "${${TREE}_${KEY}_TYPE}")
    set(path "${${TREE}_${KEY}_PATH}")

    set(arguments ${ARGN})

    if("${arguments}" STREQUAL "")
        message(FATAL_ERROR "Empty filter expression given for ${path}")
    endif()

    list(POP_FRONT arguments operator)
    string(TOUPPER "${operator}" operator)

    if(DEFINED FILE_TREE_FILTER_ALIAS_${operator})
        file_tree_filter_paths(${OUTPUT} ${TREE} "${KEY}" ${FILE_TREE_FILTER_ALIAS_${operator}} ${arguments})
        set(${OUTPUT} ${${OUTPUT}} PARENT_SCOPE)
        return()
    endif()

    if("${operator}" STREQUAL "TYPE")
        if("${arguments}" STREQUAL "")
            message(FATAL_ERROR "Empty type filter expression given for ${path}: ${ARGN}")
        endif()

        list(POP_FRONT arguments expected_type)
        string(TOUPPER "${expected_type}" expected_type)

        if(NOT FILE_TREE_NODE_TYPE_${expected_type})
            message(FATAL_ERROR "Unknown type given in the filter expression for ${path}: \"${expected_type}\"")
        endif()

        if("${type}" STREQUAL "${FILE_TREE_NODE_TYPE_${expected_type}}")
            set(${OUTPUT} TRUE PARENT_SCOPE)
            return()
        endif()
    elseif("${operator}" STREQUAL "MATCHES")
        if("${arguments}" STREQUAL "")
            message(FATAL_ERROR "Empty matching filter expression given for ${path}: ${ARGN}")
        endif()

        list(POP_FRONT arguments pattern)

        if("${path}" MATCHES "${pattern}")
            set(${OUTPUT} TRUE PARENT_SCOPE)
            return()
        endif()
    else()
        message(FATAL_ERROR "Incorrect filter expression given for ${path}: ${ARGN}")
    endif()

    set(${OUTPUT} FALSE PARENT_SCOPE)
endfunction()

macro(file_tree_check_operators)
    if(NOT "${arguments}" STREQUAL "")
        list(POP_FRONT arguments operator)

        if("${operator}" STREQUAL "|")
            file_tree_filter_paths(ACCEPTED ${TREE} "${KEY}" ${arguments})

            if(NOT ACCEPTED)
                return()
            endif()
        else()
            message(FATAL_ERROR "Unexpected arguments given to the '+' commmand for ${path}: ${ARGN}")
        endif()
    endif()
endmacro()

function(file_tree_evaluate_append RESULT TREE KEY)
    set(arguments ${ARGN})
    list(POP_FRONT arguments variable)

    file_tree_check_operators()

    set(path "${${TREE}_${KEY}_PATH}")
    append_variable(${TREE} "${variable}" "${path}")
endfunction()

function(file_tree_evaluate_assign RESULT TREE KEY)
    set(arguments ${ARGN})
    list(POP_FRONT arguments variable)

    file_tree_check_operators()

    set(path "${${TREE}_${KEY}_PATH}")
    set_variable(${TREE} "${variable}" "${path}")
    set_variable(${RESULT} ONCE TRUE)
endfunction()

function(file_tree_evaluate_cfg RESULT TREE KEY)
    set(arguments ${ARGN})
    list(POP_FRONT arguments variable)

    set(path "${${TREE}_${KEY}_PATH}")
    string(REGEX REPLACE ";\\.;" ";${path};" arguments ";${arguments};")
    string(REGEX MATCH ";(.*);" _ "${arguments}")
    set(arguments ${CMAKE_MATCH_1})

    string(TOUPPER "${variable}" variable)

    set(FILE_CFG_VARIABLES ${${TREE}_${KEY}_FILE_CFG_VARIABLES})
    list(APPEND FILE_CFG_VARIABLES "${variable}")

    set_subtree_variable(${TREE} "${KEY}" "FILE_CFG_${variable}" ${arguments})
    set_subtree_variable(${TREE} "${KEY}" FILE_CFG_VARIABLES "${FILE_CFG_VARIABLES}")

    set_variable(${RESULT} ONCE TRUE)
endfunction()

set(FILE_CFG_COMMANDS_ALIASES source_root namespace template)

foreach(ALIAS IN LISTS FILE_CFG_COMMANDS_ALIASES)
    cmake_language(EVAL CODE "
        macro(file_tree_evaluate_${ALIAS} RESULT TREE KEY)
            file_tree_evaluate_cfg(\${RESULT} \${TREE} \${KEY} \"${ALIAS}\" \${ARGN})
        endmacro()
    ")
endforeach()

#--------------------------------------------------------

function(file_tree_evaluate_file_variables TREE KEY)
    set(variables_map)
    string(REPLACE "/" ";" address "${KEY}")

    foreach(segment IN LISTS address)
        join_path(key "${key}" "${segment}")

        foreach(name IN LISTS ${TREE}_${key}_VARIABLES)
            if(NOT "${name}" IN_LIST RESERVED_VARIABLES)
                list(APPEND variables_map "${name}")
                set("VARIABLE_${name}" ${VARIABLE_${name}} ${${TREE}_${key}_${name}})
            endif()
        endforeach()
    endforeach()

    list(REMOVE_DUPLICATES "variables_map")

    foreach(name IN LISTS variables_map)
        set("${name}" ${VARIABLE_${name}} PARENT_SCOPE)
    endforeach()
endfunction()

function(file_tree_configure_file TREE KEY PATH)
    file_tree_evaluate_file_variables(${TREE} "${KEY}")
    file_cfg("${PATH}")
endfunction()

function(file_tree TREE)
    tree_set_command(${TREE} "+" file_tree_evaluate_append)
    tree_set_command(${TREE} "=" file_tree_evaluate_assign)
    tree_set_command(${TREE} "cfg" file_tree_evaluate_cfg)

    foreach(ALIAS IN LISTS FILE_CFG_COMMANDS_ALIASES)
        tree_set_command(${TREE} "${ALIAS}" "file_tree_evaluate_${ALIAS}")
    endforeach()

    tree(${TREE} ${ARGN})

    foreach(KEY IN LISTS ${TREE}_KEYS)
        if("${KEY}" MATCHES "(.+)/([0-9]+)")
            set(PARENT_KEY "${CMAKE_MATCH_1}")
            set(INDEX "${CMAKE_MATCH_2}")
        else()
            set(PARENT_KEY "")
            set(INDEX "${KEY}")
        endif()

        if("${${TREE}_${KEY}_TYPE}" STREQUAL "PARENT")
            if("${PARENT_KEY}" STREQUAL "")
                if ("${${TREE}_${KEY}_NAME}" STREQUAL "")
                    set(PATHS_${KEY} "${CMAKE_CURRENT_SOURCE_DIR}")
                else()
                    set(PATHS_${KEY} "${${TREE}_${KEY}_NAME}")
                endif()
            else()
                if("${${TREE}_${KEY}_NAME}" STREQUAL "")
                    message(FATAL_ERROR "Empty directory node name found at ${KEY}")
                endif()

                join_path(PATHS_${KEY} "${PATHS_${PARENT_KEY}}" "${${TREE}_${KEY}_NAME}")
            endif()
        elseif("${${TREE}_${KEY}_TYPE}" STREQUAL "LEAF")
            if("${${TREE}_${KEY}_NAME}" STREQUAL "")
                message(FATAL_ERROR "Empty file node name found at ${KEY}")
            endif()

            if("${PARENT_KEY}" STREQUAL "")
                join_path(path "${CMAKE_CURRENT_SOURCE_DIR}" "${${TREE}_${KEY}_NAME}")
            else()
                join_path(path "${PATHS_${PARENT_KEY}}" "${${TREE}_${KEY}_NAME}")
            endif()

            if(NOT FILE_TREE_DRY_RUN)
                normalize_path(path)
                file_tree_configure_file(${TREE} "${KEY}" "${path}")
            endif()
        else()
            message(FATAL_ERROR "Unknown file entry type at ${KEY}: ${${TREE}_${KEY}_TYPE}")
        endif()
    endforeach()

    unset_variable(${TREE} KEYS)

    promote_variables(${TREE})
endfunction()

#--------------------------------------------------------
