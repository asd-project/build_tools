
#--------------------------------------------------------
#   ASD tree utility
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

include(utils/path)
include(utils/context_variables)

#--------------------------------------------------------

function(tree_debug_step)
endfunction()

function(tree_debug_entry)
endfunction()

#--------------------------------------------------------

macro(reset_subtree_variable TREE KEY VARIABLE)
    reset_variable("${TREE}_${KEY}" "${VARIABLE}")
endmacro()

macro(set_subtree_variable TREE KEY VARIABLE)
    set_variable("${TREE}_${KEY}" "${VARIABLE}" ${ARGN})
endmacro()

macro(append_subtree_variable TREE KEY VARIABLE)
    append_variable("${TREE}_${KEY}" "${VARIABLE}" ${ARGN})
endmacro()

macro(promote_subtree_variables TREE KEY)
    promote_variables("${TREE}_${KEY}")
endmacro()

macro(evaluate_subtree_variables TREE KEY)
    evaluate_variables("${TREE}_${KEY}")
endmacro()

#--------------------------------------------------------

function(tree_combine_path OUTPUT)
    set(path)

    foreach(segment ${ARGN})
        join_path(path "${path}" "${segment}")
    endforeach()

    set(${OUTPUT} "${path}" PARENT_SCOPE)
endfunction()

#--------------------------------------------------------

function(tree_get_path OUTPUT TREE NAME)
    set(key)
    set(path)

    foreach(segment IN LISTS ${TREE}_BASE_ADDRESS)
        join_path(key "${key}" "${segment}")
        join_path(path "${path}" "${${TREE}_${key}_NAME}")
    endforeach()

    join_path(path "${path}" "${NAME}")
    normalize_path(path)

    set(${OUTPUT} "${path}" PARENT_SCOPE)
endfunction()

function(tree_set_command TREE KEY NAME)
    set("${TREE}_COMMAND_${KEY}" "${NAME}" PARENT_SCOPE)
endfunction()

function(tree_evaluate_command RESULT TREE KEY COMMAND)
    if(NOT DEFINED "${TREE}_COMMAND_${COMMAND}")
        message(FATAL_ERROR "Unknown command given: ${COMMAND}")
    endif()

    set(command_function "${${TREE}_COMMAND_${COMMAND}}")

    cmake_language(CALL ${command_function} ${RESULT} ${TREE} "${KEY}" ${ARGN})

    promote_variables(${TREE})
    promote_subtree_variables(${TREE} "${KEY}")

    promote_variables(${RESULT})
endfunction()

function(tree_evaluate_node TREE TYPE KEY)
    set(address ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})

    tree_get_path(path ${TREE} "${${TREE}_${KEY}_NAME}")

    set_subtree_variable(${TREE} "${KEY}" TYPE "${TYPE}")
    set_subtree_variable(${TREE} "${KEY}" PATH "${path}")

    set(key)
    set(commands)

    foreach(segment IN LISTS address)
        join_path(key "${key}" "${segment}")

        set("REMOVED_COMMANDS_${key}")

        foreach(command_expression IN LISTS ${TREE}_${key}_COMMANDS)
            string(REPLACE " " ";" command "${command_expression}")

            set(RESULT_ONCE)

            tree_evaluate_command(RESULT ${TREE} "${KEY}" ${command})

            if(RESULT_ONCE)
                list(APPEND "REMOVED_COMMANDS_${key}" "${command_expression}")
            endif()
        endforeach()

        promote_subtree_variables(${TREE} "${key}")
    endforeach()

    append_variable(${TREE} KEYS "${KEY}")
    tree_debug_entry(${TREE} "${KEY}")

    set(key)

    foreach(segment IN LISTS address)
        join_path(key "${key}" "${segment}")

        if(NOT "${REMOVED_COMMANDS_${key}}" STREQUAL "")
            foreach(command_expression IN LISTS "REMOVED_COMMANDS_${key}")
                list(REMOVE_ITEM "${TREE}_${key}_COMMANDS" "${command_expression}")
            endforeach()
        endif()

        promote_subtree_variables(${TREE} "${key}")
    endforeach()

    promote_variables(${TREE})
endfunction()

function(tree_evaluate_leaf TREE)
    if("${${TREE}_CURRENT_INDEX}" STREQUAL "")
        message(FATAL_ERROR "[tree] Current index is not defined")
    endif()

    if(NOT "${${TREE}_CURRENT_NAME}" STREQUAL "")
        # leaf found
        tree_combine_path(key ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
        set_subtree_variable(${TREE} "${key}" NAME "${${TREE}_CURRENT_NAME}")
        tree_evaluate_node(${TREE} LEAF "${key}")

        reset_variable(${TREE} CURRENT_NAME)

        math(EXPR ${TREE}_CURRENT_INDEX "${${TREE}_CURRENT_INDEX} + 1")

        promote_variables(${TREE})
        promote_subtree_variables(${TREE} "${key}")
    endif()
endfunction()

function(tree_evaluate_parent TREE)
    if("${${TREE}_CURRENT_INDEX}" STREQUAL "")
        message(FATAL_ERROR "[tree] Current index is not defined")
    endif()

    if(NOT "${${TREE}_CURRENT_NAME}" STREQUAL "")
        # parent found
        tree_combine_path(key ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
        set_subtree_variable(${TREE} "${key}" NAME "${${TREE}_CURRENT_NAME}")
        tree_evaluate_node(${TREE} PARENT "${key}")

        reset_variable(${TREE} CURRENT_NAME)

        promote_variables(${TREE})
        promote_subtree_variables(${TREE} "${key}")
    elseif("${${TREE}_BASE_ADDRESS}" STREQUAL "")
        # implicit root
        tree_combine_path(key ${${TREE}_CURRENT_INDEX})
        set_subtree_variable(${TREE} "${key}" NAME "")
        tree_evaluate_node(${TREE} PARENT "${key}")

        promote_variables(${TREE})
        promote_subtree_variables(${TREE} "${key}")
    endif()
endfunction()

#--------------------------------------------------------

function(tree_start TREE)
    tree_debug_step(${TREE} "STR")

    if("${${TREE}_CURRENT_INDEX}" STREQUAL "")
        reset_variable(${TREE} BASE_ADDRESS)
        set_variable(${TREE} CURRENT_INDEX 0)
        reset_variable(${TREE} CURRENT_NAME)
    endif()
endfunction()

function(tree_end TREE)
    tree_combine_path(key ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
    tree_evaluate_leaf(${TREE})

    if(NOT "${${TREE}_BASE_ADDRESS}" STREQUAL "")
        message(FATAL_ERROR "Unexpected tree end at ${key}")
    endif()

    tree_debug_step(${TREE} "END")

    promote_variables(${TREE})
    promote_subtree_variables(${TREE} "${key}")
endfunction()

function(tree_push TREE)
    tree_combine_path(key ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
    tree_evaluate_parent(${TREE})

    append_variable(${TREE} BASE_ADDRESS "${${TREE}_CURRENT_INDEX}")
    set_variable(${TREE} CURRENT_INDEX 0)

    tree_debug_step(${TREE} "PSH")

    promote_variables(${TREE})
    promote_subtree_variables(${TREE} "${key}")
endfunction()

function(tree_pop TREE)
    tree_combine_path(key ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
    tree_evaluate_leaf(${TREE})

    list(POP_BACK ${TREE}_BASE_ADDRESS ${TREE}_CURRENT_INDEX)

    tree_debug_step(${TREE} "POP")

    math(EXPR ${TREE}_CURRENT_INDEX "${${TREE}_CURRENT_INDEX} + 1")

    promote_variables(${TREE})
    promote_subtree_variables(${TREE} "${key}")
endfunction()

function(tree_next TREE NAME)
    tree_combine_path(key ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
    tree_evaluate_leaf(${TREE})

    set_variable(${TREE} CURRENT_NAME "${NAME}")

    tree_debug_step(${TREE} "NXT" "${NAME}")

    promote_variables(${TREE})
    promote_subtree_variables(${TREE} "${key}")
endfunction()

function(tree_append_command TREE EXPRESSION)
    tree_combine_path(key ${${TREE}_BASE_ADDRESS} ${${TREE}_CURRENT_INDEX})
    tree_evaluate_leaf(${TREE})

    string(REGEX MATCH "\\[;?(.*[^;]);?\\]" _ "${EXPRESSION}")
    string(REPLACE ";" " " command "${CMAKE_MATCH_1}")

    if ("${command}" STREQUAL "")
        message(FATAL_ERROR "Invalid tree command: ${EXPRESSION}")
    endif()

    append_subtree_variable(${TREE} "${key}" COMMANDS "${command}")

    promote_variables(${TREE})
    promote_subtree_variables(${TREE} "${key}")
endfunction()

#--------------------------------------------------------

function(tree TREE)
    tree_start(${TREE})

    foreach(entry ${ARGN})
        if("${entry}" MATCHES "^\\[")
            tree_append_command(${TREE} "${entry}")
            continue()
        endif()

        if("${entry}" STREQUAL "{")
            tree_push(${TREE})
            continue()
        endif()

        if("${entry}" STREQUAL "}")
            tree_pop(${TREE})
            continue()
        endif()

        if("${entry}" STREQUAL "{}")
            tree_push(${TREE})
            tree_pop(${TREE})
            continue()
        endif()

        tree_next(${TREE} "${entry}")
    endforeach()

    tree_end(${TREE})

    promote_variables(${TREE})

    foreach(KEY IN LISTS ${TREE}_KEYS)
        promote_subtree_variables(${TREE} "${KEY}")
    endforeach()
endfunction()

#--------------------------------------------------------
