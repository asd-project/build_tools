
#--------------------------------------------------------
#   ASD context variables support
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include_guard(GLOBAL)

#--------------------------------------------------------

macro(reset_variable CONTEXT VARIABLE)
    set("${CONTEXT}_${VARIABLE}")
    set("${CONTEXT}_${VARIABLE}" ${${CONTEXT}_${VARIABLE}} PARENT_SCOPE)

    set("${CONTEXT}_VARIABLES" ${${CONTEXT}_VARIABLES} "${VARIABLE}")
    list(REMOVE_DUPLICATES "${CONTEXT}_VARIABLES")

    set("${CONTEXT}_VARIABLES" ${${CONTEXT}_VARIABLES} PARENT_SCOPE)
endmacro()

macro(set_variable CONTEXT VARIABLE)
    set("${CONTEXT}_${VARIABLE}" ${ARGN})
    set("${CONTEXT}_${VARIABLE}" ${${CONTEXT}_${VARIABLE}} PARENT_SCOPE)

    set("${CONTEXT}_VARIABLES" ${${CONTEXT}_VARIABLES} "${VARIABLE}")
    list(REMOVE_DUPLICATES "${CONTEXT}_VARIABLES")

    set("${CONTEXT}_VARIABLES" ${${CONTEXT}_VARIABLES} PARENT_SCOPE)
endmacro()

macro(unset_variable CONTEXT VARIABLE)
    set("${CONTEXT}_${VARIABLE}")
    set("${CONTEXT}_${VARIABLE}" PARENT_SCOPE)

    list(REMOVE_ITEM "${CONTEXT}_VARIABLES" "${VARIABLE}")

    set("${CONTEXT}_VARIABLES" ${${CONTEXT}_VARIABLES} PARENT_SCOPE)
endmacro()

macro(append_variable CONTEXT VARIABLE)
    set("${CONTEXT}_${VARIABLE}" ${${CONTEXT}_${VARIABLE}} ${ARGN})
    set("${CONTEXT}_${VARIABLE}" ${${CONTEXT}_${VARIABLE}} PARENT_SCOPE)

    set("${CONTEXT}_VARIABLES" ${${CONTEXT}_VARIABLES} "${VARIABLE}")
    list(REMOVE_DUPLICATES "${CONTEXT}_VARIABLES")

    set("${CONTEXT}_VARIABLES" ${${CONTEXT}_VARIABLES} PARENT_SCOPE)
endmacro()

macro(promote_variables CONTEXT)
    set("${CONTEXT}_VARIABLES" ${${CONTEXT}_VARIABLES} PARENT_SCOPE)

    foreach(VARIABLE IN LISTS "${CONTEXT}_VARIABLES")
        set("${CONTEXT}_${VARIABLE}" ${${CONTEXT}_${VARIABLE}} PARENT_SCOPE)
    endforeach()
endmacro()

macro(unset_variables CONTEXT)
    foreach(VARIABLE IN LISTS "${CONTEXT}_VARIABLES")
        set("${CONTEXT}_${VARIABLE}")
        set("${CONTEXT}_${VARIABLE}" PARENT_SCOPE)
    endforeach()

    set("${CONTEXT}_VARIABLES")
    set("${CONTEXT}_VARIABLES" PARENT_SCOPE)
endmacro()

macro(evaluate_variables CONTEXT)
    foreach(VARIABLE IN LISTS "${CONTEXT}_VARIABLES")
        set(${VARIABLE} ${${CONTEXT}_${VARIABLE}} PARENT_SCOPE)
    endforeach()
endmacro()

macro(dump_variables CONTEXT)
    foreach(VARIABLE IN LISTS "${CONTEXT}_VARIABLES")
        message("${VARIABLE}=${${CONTEXT}_${VARIABLE}}")
    endforeach()
endmacro()

#--------------------------------------------------------
