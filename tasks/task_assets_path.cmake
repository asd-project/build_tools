function(do target path)
    file(GLOB_RECURSE assets_files LIST_DIRECTORIES false RELATIVE "${path}" "${path}/*")

    foreach(file ${assets_files})
        set(INPUT_FILE "${path}/${file}")
        set(OUTPUT_FILE "$<TARGET_FILE_DIR:${target}>/${file}")

        add_custom_command(
            TARGET ${target} POST_BUILD
            DEPENDS "${INPUT_FILE}"
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${INPUT_FILE}" "${OUTPUT_FILE}"
            COMMENT "Copy ${INPUT_FILE}...\r\n"
            VERBATIM
        )
    endforeach()
endfunction()