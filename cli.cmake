#--------------------------------------------------------
#   ASD command-line interface
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

#------------------------------------------------------------------------------

set(FORCE_COLORIZE ON)

include("${CMAKE_CURRENT_LIST_DIR}/setup.cmake")

#------------------------------------------------------------------------------

function(print_usage)
    message("> asd::project_management_utility::print_usage()")
    message("Usage:")
    message("    configure [-t=<debug|release>] [-m|--multi] [-p=<conan profile=default conan profile>] [-g=<cmake generator>] [-u|--unity] - Configure workspace")
    message("    create <inline|library|application|external|test|example|benchmark|tool> <package-name> - Create new package(header-only/static library, application or external package)")
    message("    build [<target name>] [-t=<debug|release>] [-p=<conan profile=default conan profile>] [-- build system args...] - Build specific target")
    message("    run <target name> [-t=<debug|release>] [-p=<conan profile=default conan profile>] [-- build system args...] [--- application args] - Run specific target")
    message("    commit [<paths to packages>...] - Send oackage(s) to the local conan cache. Packages should be built before commiting")
    message("    update [<paths to packages>...] - Regenerate package(s) structure")
    message("    root - Get asd root")
endfunction()

separate_arguments(ARGS UNIX_COMMAND "${ARGS}")
get_filename_component(WORKSPACE_DIR "${WORKSPACE_DIR}" ABSOLUTE)

if("${ARGS}" STREQUAL "")
    print_usage()
    return()
endif()

list(GET ARGS 0 COMMAND)
list(REMOVE_AT ARGS 0)

if("${COMMAND}" STREQUAL "root")
    execute_process(COMMAND ${CMAKE_COMMAND} -E echo_append "${ROOT}")
elseif("${COMMAND}" STREQUAL "configure")
    include(commands/configure)
    execute(${ARGS})
elseif("${COMMAND}" STREQUAL "create")
    include(commands/create)
    execute(${ARGS})
elseif("${COMMAND}" STREQUAL "clean")
    include(commands/clean)
    execute(${ARGS})
elseif("${COMMAND}" STREQUAL "build")
    include(commands/build)
    execute(${ARGS})
elseif("${COMMAND}" STREQUAL "run")
    include(commands/run)
    execute(${ARGS})
elseif("${COMMAND}" STREQUAL "commit")
    include(commands/commit)
    execute(${ARGS})
elseif("${COMMAND}" STREQUAL "update")
    include(commands/update)
    execute(${ARGS})
elseif("${COMMAND}" STREQUAL "self_test")
    include(commands/self_test)
    execute(${ARGS})
else()
    print_usage()
endif()
