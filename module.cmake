#--------------------------------------------------------
#   ASD cmake module facilities
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

#--------------------------------------------------------

include(utils/message)
include(utils/path)

file(RELATIVE_PATH PACKAGE_DIR "${WORKSPACE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")

color_message(BOLD "${MESSAGE_ENTER_STATUS} " GREEN UNDERLINE "${PACKAGE_DIR}/")

if(CHECK_UNLISTED_FILES OR REMOVE_UNLISTED_FILES)
    set(DIRECTORY_REGISTERED_FILES "" CACHE INTERNAL "" FORCE)
    cmake_language(DEFER DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" CALL check_unlisted_files)
endif()

cmake_language(DEFER DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" CALL message "")

#--------------------------------------------------------

include_guard(GLOBAL)

if(CHECK_UNLISTED_FILES OR REMOVE_UNLISTED_FILES)
    cmake_language(DEFER DIRECTORY "${CMAKE_SOURCE_DIR}" CALL report_all_unlisted_files)
endif()

include(utils/text)
include(utils/file)
include(platform/osx)
include(platform/emscripten)

set(MODULE_TYPES LIBRARY;STATIC;INLINE;SHARED;APPLICATION;TEST;EXAMPLE;BENCHMARK CACHE INTERNAL "Module types" FORCE)

if(WIN32)
    set(CMAKE_SHARED_LIBRARY_PREFIX "" CACHE INTERNAL "Cmake shared lib prefix" FORCE)
endif()

function(set_output_dir TARGET DIR TYPE)
    get_filename_component(DIR "${DIR}" REALPATH)

    if(BUILD_TYPE AND "${BUILD_TYPE}" STREQUAL "Multi")
        set_target_properties(${TARGET} PROPERTIES
            ${TYPE}_OUTPUT_DIRECTORY "${DIR}"
            ${TYPE}_OUTPUT_DIRECTORY_RELEASE "${DIR}/Release"
            ${TYPE}_OUTPUT_DIRECTORY_DEBUG "${DIR}/Debug"
            ${TYPE}_OUTPUT_DIRECTORY_RELWITHDEBINFO "${DIR}/RelWithDebInfo"
            ${TYPE}_OUTPUT_DIRECTORY_MINSIZEREL "${DIR}/MinSizeRel"
        )
    else()
        set_target_properties(${TARGET} PROPERTIES
            ${TYPE}_OUTPUT_DIRECTORY "${DIR}"
            ${TYPE}_OUTPUT_DIRECTORY_RELEASE "${DIR}"
            ${TYPE}_OUTPUT_DIRECTORY_DEBUG "${DIR}"
            ${TYPE}_OUTPUT_DIRECTORY_RELWITHDEBINFO "${DIR}"
            ${TYPE}_OUTPUT_DIRECTORY_MINSIZEREL "${DIR}"
        )
    endif()
endfunction()

function(module type)
    set(options EXTERNAL)
    set(single_value ROOT NAMESPACE)
    set(multi_value)

    cmake_parse_arguments(ARGS "${options}" "${single_value}" "${multi_value}" ${ARGN})

    list(LENGTH ARGS_UNPARSED_ARGUMENTS POSITIONAL_ARGUMENTS_COUNT)

    if("${POSITIONAL_ARGUMENTS_COUNT}" EQUAL 1)
        set(MODULE_NAME "${ARGS_UNPARSED_ARGUMENTS}")
    elseif("${POSITIONAL_ARGUMENTS_COUNT}" EQUAL 0)
        set(MODULE_NAME "${PROJECT_NAME}")
    else()
        message(FATAL_ERROR "Unexpected positional arguments to module()")
    endif()

    if(ARGS_EXTERNAL)
        set(MODULE_EXTERNAL TRUE)
    else()
        set(MODULE_EXTERNAL FALSE)
    endif()

    if(ARGS_ROOT)
        get_filename_component(MODULE_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/${ARGS_ROOT}" REALPATH)
        unset(ARGS_ROOT)
    else()
        get_filename_component(MODULE_ROOT "${CMAKE_CURRENT_SOURCE_DIR}" REALPATH)
    endif()

    if(ARGS_NAMESPACE)
        set(MODULE_NAMESPACE "${ARGS_NAMESPACE}")
        unset(ARGS_NAMESPACE)
    else()
        set(MODULE_NAMESPACE)
    endif()

    set(MODULE_NAME "${MODULE_NAME}" PARENT_SCOPE)
    set(MODULE_ROOT "${MODULE_ROOT}" PARENT_SCOPE)
    set(MODULE_NAMESPACE "${MODULE_NAMESPACE}" PARENT_SCOPE)
    set(MODULE_EXTERNAL ${MODULE_EXTERNAL} PARENT_SCOPE)

    set(${MODULE_NAME}_SOURCES CACHE INTERNAL "${MODULE_NAME} sources" FORCE)
    set(${MODULE_NAME}_HEADERS CACHE INTERNAL "${MODULE_NAME} headers" FORCE)
    set(${MODULE_NAME}_INCLUDE_DIRECTORIES CACHE INTERNAL "${MODULE_NAME} include directories" FORCE)
    set(${MODULE_NAME}_SYSTEM_INCLUDE_DIRECTORIES CACHE INTERNAL "${MODULE_NAME} system include directories" FORCE)
    set(${MODULE_NAME}_LINK_LIBRARIES CACHE INTERNAL "${MODULE_NAME} linked libraries" FORCE)
    set(${MODULE_NAME}_COMPILE_OPTIONS CACHE INTERNAL "${MODULE_NAME} compile options" FORCE)
    set(${MODULE_NAME}_LINK_OPTIONS CACHE INTERNAL "${MODULE_NAME} link options" FORCE)
    set(${MODULE_NAME}_PROPERTIES CACHE INTERNAL "${MODULE_NAME} properties" FORCE)
    set(${MODULE_NAME}_TASKS CACHE INTERNAL "${MODULE_NAME} tasks" FORCE)
    set(${MODULE_NAME}_CUSTOM_DEPENDENCIES CACHE INTERNAL "${MODULE_NAME} custom dependencies" FORCE)

    if("${type}" STREQUAL "")
        message(FATAL_ERROR "Incorrect usage of 'module' function! Type of module is not set. List of correct types: (${MODULE_TYPES})")
    elseif(NOT ";${MODULE_TYPES};" MATCHES ";${type};")
        message(FATAL_ERROR "Incorrect usage of 'module' function! List of correct types: (${MODULE_TYPES}). Provided type is ${type}")
    endif()

    set(${MODULE_NAME}_MODULE_TYPE ${type} CACHE STRING "${MODULE_NAME} module type" FORCE)

    set(CURRENT_SOURCE_DOMAIN "." CACHE STRING "" FORCE)
    set(CURRENT_SOURCE_GROUP_PATH src CACHE STRING "" FORCE)
    set(CURRENT_SOURCE_GROUP_NAME Sources CACHE STRING "" FORCE)

    if("${type}" STREQUAL "APPLICATION")
        set(${MODULE_NAME}_FOLDER "apps" CACHE STRING "${MODULE_NAME} folder" FORCE)
        set(MODULE_TYPE_TEXT "app")
    elseif("${type}" STREQUAL "TEST")
        set(${MODULE_NAME}_FOLDER "tests" CACHE STRING "${MODULE_NAME} folder" FORCE)
        set(MODULE_TYPE_TEXT "test app")
    elseif("${type}" STREQUAL "EXAMPLE")
        set(${MODULE_NAME}_FOLDER "examples" CACHE STRING "${MODULE_NAME} folder" FORCE)
        set(MODULE_TYPE_TEXT "example")
    elseif("${type}" STREQUAL "BENCHMARK")
        set(${MODULE_NAME}_FOLDER "benchmarks" CACHE STRING "${MODULE_NAME} folder" FORCE)
        set(MODULE_TYPE_TEXT "benchmark")
    else()
        set(${MODULE_NAME}_FOLDER "libs" CACHE STRING "${MODULE_NAME} folder" FORCE)
        set(MODULE_TYPE_TEXT "library")
    endif()

    if(REGENERATE_PACKAGES)
        color_message(BOLD CYAN "${MESSAGE_UPDATE_STATUS} " WHITE "Update ${MODULE_TYPE_TEXT} " CYAN BOLD "\"${MODULE_NAME}\"" RESET "...")
        return()
    endif()

    set(BINARY_OUTPUT_ROOT "${CMAKE_CURRENT_BINARY_DIR}/bin")
    get_filename_component(BINARY_OUTPUT_ROOT "${BINARY_OUTPUT_ROOT}" REALPATH)
    set(${MODULE_NAME}_BINARY_OUTPUT "${BINARY_OUTPUT_ROOT}" CACHE PATH "Binary output" FORCE)

    color_message(BOLD CYAN "${MESSAGE_UPDATE_STATUS} " WHITE "Configure ${MODULE_TYPE_TEXT} " CYAN BOLD "\"${MODULE_NAME}\"" RESET "...")
endfunction()

function(group path name)
    if("${path}" STREQUAL "")
        message(FATAL_ERROR "The group path may not be empty!")
    endif()

    string(REPLACE / \\ name ${name})
    set(CURRENT_SOURCE_GROUP_PATH ${path} CACHE STRING "" FORCE)
    set(CURRENT_SOURCE_GROUP_NAME ${name} CACHE STRING "" FORCE)

    if(NOT ";${${MODULE_NAME}_SOURCE_GROUPS};" MATCHES ";${path};")
        set(${MODULE_NAME}_SOURCE_GROUPS ${${MODULE_NAME}_SOURCE_GROUPS} "${path}" CACHE INTERNAL "" FORCE)

        if(DEFINED DIRECTORY_REGISTERED_FILES)
            if(NOT "${path}" MATCHES "\\.\\./") # don't check external files
                set(GROUPS ${DIRECTORY_SOURCE_GROUPS} "${MODULE_ROOT}/${path}")
                list(REMOVE_DUPLICATES GROUPS)
                set(DIRECTORY_SOURCE_GROUPS ${GROUPS} CACHE INTERNAL "" FORCE)
            endif()
        endif()
    endif()
endfunction()

function(domain path)
    string(REPLACE \\ / path ${path})
    set(CURRENT_SOURCE_DOMAIN ${path} CACHE STRING "" FORCE)
endfunction()

function(flush_sources dir)
    if(NOT "${ARGN}" STREQUAL "")
        string(REGEX REPLACE "^\\./" "" dir "${dir}")
        string(REGEX REPLACE "/$" "" dir "${dir}")
        string(REPLACE "/" "\\" dir "${dir}")

        if("${dir}" STREQUAL "")
            source_group(${CURRENT_SOURCE_GROUP_NAME} FILES ${ARGN})
        else()
            source_group(${CURRENT_SOURCE_GROUP_NAME}\\${dir} FILES ${ARGN})
        endif()

        set(CURRENT_SOURCE_LIST ${CURRENT_SOURCE_LIST} ${ARGN} CACHE INTERNAL "" FORCE)
    endif()
endfunction()

function(rename_path root source_path target_path)
    if(EXISTS "${root}/${target_path}")
        if(NOT EXISTS "${root}/${source_path}")
            color_message(YELLOW BOLD "${MESSAGE_WARNING_STATUS} " RESET "${source_path} is already moved when trying to rename to " WHITE BOLD "${target_path}")
        else()
            message(FATAL_ERROR "Can't rename ${source_path} to ${target_path} - file already exists")
        endif()
    else()
        if(EXISTS "${root}/${source_path}")
            color_message(GREEN BOLD "${MESSAGE_ADD_STATUS} " RESET "Rename ${source_path} to " WHITE BOLD "${root}/${target_path}")
            get_filename_component(target_directory "${root}/${target_path}" DIRECTORY)
            file(MAKE_DIRECTORY "${target_directory}")
            file(RENAME "${root}/${source_path}" "${root}/${target_path}")
        else()
            message(FATAL_ERROR "Original file ${root}/${source_path} doesn't exist, cannot create ${source}!")
        endif()
    endif()
endfunction()

function(collect_files out)
    set(source_list)
    set(dir)
    set(CURRENT_SOURCE_LIST CACHE INTERNAL "" FORCE)
    set(root "${MODULE_ROOT}/${CURRENT_SOURCE_GROUP_PATH}")

    set(record_command)
    set(command_arguments)
    set(renamed_path)

    foreach(entry ${ARGN})
        if(record_command)
            if("${entry}" STREQUAL "%")
                message(FATAL_ERROR "Incorrect file command statement - found repeating %")
            endif()

            if("${entry}" STREQUAL ">")
                list(LENGTH command_arguments commands_count)

                if(NOT ${commands_count} EQUAL 1)
                    message(FATAL_ERROR "Incorrect number of files given in renaming command statement (${command_arguments})")
                endif()

                join_path(renamed_path "${CURRENT_SOURCE_DOMAIN}/${dir}" "${command_arguments}")

                set(record_command)
                set(command_arguments)
            else()
                list(APPEND command_arguments "${entry}")
            endif()

            continue()
        endif()

        if("${entry}" MATCHES ".*/$" OR "${entry}" MATCHES ".*\\$")
            flush_sources("${CURRENT_SOURCE_DOMAIN}/${dir}" ${dir_list})
            set(dir_list)

            string(REPLACE "\\" "/" entry ${entry})
            string(REGEX REPLACE "/$" "" entry ${entry})

            if("${dir}" STREQUAL "")
                set(dir "${entry}")
            else()
                set(dir "${dir}/${entry}")
            endif()

            string(REPLACE "//" "/" dir "${dir}")

            if(NOT "${renamed_path}" STREQUAL "")
                rename_path("${root}" "${renamed_path}" "${CURRENT_SOURCE_DOMAIN}/${dir}")
                set(renamed_path)
            endif()
        elseif("${entry}" STREQUAL "..")
            flush_sources("${CURRENT_SOURCE_DOMAIN}/${dir}" ${dir_list})
            set(dir_list)
            get_filename_component(dir "${dir}" DIRECTORY)

            if("${dir}" STREQUAL "/")
                set(dir)
            endif()
        elseif("${entry}" STREQUAL ".")
            flush_sources("${CURRENT_SOURCE_DOMAIN}/${dir}" ${dir_list})
            set(dir_list)
            set(dir)
        elseif("${entry}" STREQUAL "%")
            set(record_command TRUE)
            set(command_arguments)
        elseif("${entry}" STREQUAL ">")
            message(FATAL_ERROR "Incorrect files list - unexpected > found")
        else()
            join_path(source "${CURRENT_SOURCE_DOMAIN}/${dir}" "${entry}")

            if(NOT "${renamed_path}" STREQUAL "")
                rename_path("${root}" "${renamed_path}" "${source}")
                set(renamed_path)
            elseif(NOT EXISTS "${root}/${source}")
                create_source(${root} ${source})
            endif()

            list(APPEND dir_list "${root}/${source}")
        endif()
    endforeach()

    if(NOT "${renamed_path}" STREQUAL "")
        message(FATAL_ERROR "No destination file after renaming statement (% ${renamed_path} >)")
    endif()

    if(record_command)
        message(FATAL_ERROR "Incorrect file command statement (%) - no command found. Arguments are: ${command_arguments}")
    endif()

    flush_sources("${CURRENT_SOURCE_DOMAIN}/${dir}" ${dir_list})
    set(${out} ${CURRENT_SOURCE_LIST} PARENT_SCOPE)

    if("${CURRENT_SOURCE_GROUP_PATH}" STREQUAL "include")
        set(${MODULE_NAME}_HEADERS ${${MODULE_NAME}_HEADERS} ${CURRENT_SOURCE_LIST} CACHE INTERNAL "${MODULE_NAME} headers" FORCE)
    endif()

    set(${MODULE_NAME}_SOURCES ${${MODULE_NAME}_SOURCES} ${CURRENT_SOURCE_LIST} CACHE INTERNAL "${MODULE_NAME} sources" FORCE)
    unset(CURRENT_SOURCE_LIST CACHE)
endfunction()

function(files)
    collect_files(_ ${ARGN})
endfunction()

function(module_add_sources)
    set(${MODULE_NAME}_SOURCES ${${MODULE_NAME}_SOURCES} ${ARGN} CACHE INTERNAL "${MODULE_NAME} sources" FORCE)
endfunction()

function(module_register_files)
    if(DEFINED DIRECTORY_REGISTERED_FILES)
        set(FILES ${DIRECTORY_REGISTERED_FILES} ${ARGN})
        list(REMOVE_DUPLICATES FILES)
        set(DIRECTORY_REGISTERED_FILES ${FILES} CACHE INTERNAL "Registered files" FORCE)
    endif()
endfunction()

function(module_create_source path contents)
    file(WRITE "${path}" "${contents}")
    module_add_sources("${path}")
endfunction()

function(module_include_directories)
    set(options SYSTEM)
    set(single_value)
    set(multi_value)

    cmake_parse_arguments(MODULE_INCLUDE "${options}" "${single_value}" "${multi_value}" ${ARGN})

    if(MODULE_INCLUDE_SYSTEM)
        set(DIRS ${${MODULE_NAME}_SYSTEM_INCLUDE_DIRECTORIES};${MODULE_INCLUDE_UNPARSED_ARGUMENTS})
        list(REMOVE_DUPLICATES DIRS)
        set(${MODULE_NAME}_SYSTEM_INCLUDE_DIRECTORIES ${DIRS} CACHE INTERNAL "${MODULE_NAME} system include directories" FORCE)
    else()
        set(DIRS ${${MODULE_NAME}_INCLUDE_DIRECTORIES};${MODULE_INCLUDE_UNPARSED_ARGUMENTS})
        list(REMOVE_DUPLICATES DIRS)
        set(${MODULE_NAME}_INCLUDE_DIRECTORIES ${DIRS} CACHE INTERNAL "${MODULE_NAME} include directories" FORCE)
    endif()
endfunction()

function(module_link_libraries)
    set(LIBS ${${MODULE_NAME}_LINK_LIBRARIES})

    foreach(LIB ${ARGN})
        if(TARGET "${LIB}.interface")
            list(APPEND LIBS "${LIB}.interface")
        else()
            list(APPEND LIBS ${LIB})
        endif()
    endforeach()

    list(REMOVE_DUPLICATES LIBS)
    set(${MODULE_NAME}_LINK_LIBRARIES ${LIBS} CACHE INTERNAL "${MODULE_NAME} linked libraries" FORCE)
endfunction()

function(module_compile_options)
    set(OPTIONS ${${MODULE_NAME}_COMPILE_OPTIONS};${ARGN})
    list(REMOVE_DUPLICATES OPTIONS)
    set(${MODULE_NAME}_COMPILE_OPTIONS ${OPTIONS} CACHE INTERNAL "${MODULE_NAME} compile options" FORCE)
endfunction()

function(module_link_options)
    set(OPTIONS ${${MODULE_NAME}_LINK_OPTIONS};${ARGN})
    list(REMOVE_DUPLICATES OPTIONS)
    set(${MODULE_NAME}_LINK_OPTIONS ${OPTIONS} CACHE INTERNAL "${MODULE_NAME} link options" FORCE)
endfunction()

function(set_module_properties)
    set(${MODULE_NAME}_PROPERTIES ${${MODULE_NAME}_PROPERTIES};${ARGN} CACHE INTERNAL "${MODULE_NAME} properties" FORCE)
endfunction()

function(module_task task)
    set(task_string "${task}")

    foreach(arg ${ARGN})
        set(task_string "${task_string} \"${arg}\"")
    endforeach()

    set(${MODULE_NAME}_TASKS ${${MODULE_NAME}_TASKS};${task_string} CACHE INTERNAL "${MODULE_NAME} tasks" FORCE)
endfunction()

function(module_custom_dependencies)
    set(${MODULE_NAME}_CUSTOM_DEPENDENCIES ${${MODULE_NAME}_CUSTOM_DEPENDENCIES} ${ARGN} CACHE INTERNAL "${MODULE_NAME} custom dependencies" FORCE)
endfunction()

# create custom trigger to relink executable after change in given files
function(module_link_files target)
    if(NOT EXISTS "${CMAKE_CURRENT_BINARY_DIR}/generated")
        file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/generated")
    endif()

    string(REPLACE ";" "\n// " FILES_LIST "${ARGN}")
    set(FILE_NAME "${CMAKE_CURRENT_BINARY_DIR}/generated/${target}.cpp")
    module_create_source("${FILE_NAME}" "// ${FILES_LIST}")
    source_group(Linked FILES "${FILE_NAME}")

    set_source_files_properties("${FILE_NAME}" DIRECTORY "${CURRENT_SOURCE_DIR}" PROPERTIES OBJECT_DEPENDS "${ARGN}")
endfunction()

#--------------------------------------------------------

function(export_api domain)
    set(${MODULE_NAME}_API_DOMAIN ${domain} CACHE STRING "${MODULE_NAME} api domain" FORCE)
endfunction()

#   endmodule function.

function(execute_task TARGET_NAME TASK)
    include("${TASK}")
    do("${TARGET_NAME}" ${ARGN})
endfunction()

function(setup_module_target MODULE_NAME)
    #--------------------------------------------------------
    # Register project files to track changes

    register_project_files(PROJECT_FILES)

    if(PROJECT_FILES)
        list(APPEND SOURCES "${PROJECT_FILES}")
        set_property(DIRECTORY "${CMAKE_SOURCE_DIR}" APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS "${PROJECT_FILES}")
    endif()

    #--------------------------------------------------------
    # CMake target

    set(TARGET_NAME "${MODULE_NAME}")

    if("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "STATIC" OR (NOT USE_SHARED_LIBRARIES AND "${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "LIBRARY"))
        add_library("${TARGET_NAME}" STATIC ${SOURCES})
    elseif("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "INLINE")
        add_library("${TARGET_NAME}" INTERFACE ${SOURCES})
    elseif("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "SHARED" OR (USE_SHARED_LIBRARIES AND "${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "LIBRARY"))
        add_library("${TARGET_NAME}" SHARED ${SOURCES})

        if(APPLE AND("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang"))
            target_compile_options("${TARGET_NAME}" PRIVATE -dynamiclib)
        endif()
    elseif("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "APPLICATION")
        add_executable("${TARGET_NAME}" ${SOURCES})
    elseif("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "TEST")
        add_executable("${TARGET_NAME}" ${SOURCES})
        add_test(NAME "${TARGET_NAME}" COMMAND "${TARGET_NAME}")
    elseif("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "EXAMPLE")
        add_executable("${TARGET_NAME}" ${SOURCES})
    elseif("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "BENCHMARK")
        add_executable("${TARGET_NAME}" ${SOURCES})
    elseif("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "")
        message(FATAL_ERROR "Module type is not set! Call 'module' first!")
    else()
        message(FATAL_ERROR "Module type is incorrect! Check the 'module' function call")
    endif()

    list(LENGTH SOURCES SOURCES_COUNT)

    if(${SOURCES_COUNT} EQUAL 0)
        color_message(WHITE "   No sources registered")
    elseif(${SOURCES_COUNT} EQUAL 1)
        color_message(WHITE "   1 source file registered")
    else()
        color_message(WHITE "   ${SOURCES_COUNT} source files registered")
    endif()

    #--------------------------------------------------------
    # Custom tasks

    foreach(task_string ${${MODULE_NAME}_TASKS})
        separate_arguments(TASK_ARGS UNIX_COMMAND ${task_string})
        execute_task("${TARGET_NAME}" ${TASK_ARGS})
    endforeach()

    #--------------------------------------------------------
    # Setup options and properties

    set_output_dir("${MODULE_NAME}" "${CMAKE_CURRENT_BINARY_DIR}/lib" ARCHIVE)
    set_output_dir("${MODULE_NAME}" "${CMAKE_CURRENT_BINARY_DIR}/lib" LIBRARY)
    set_output_dir("${MODULE_NAME}" "${CMAKE_CURRENT_BINARY_DIR}/bin" RUNTIME)

    if("${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "INLINE")
        set(TARGET_SCOPE INTERFACE)
    else()
        set(TARGET_SCOPE PUBLIC)
    endif()

    set_target_properties("${MODULE_NAME}" PROPERTIES FOLDER ${${MODULE_NAME}_FOLDER})

    foreach(HEADER ${${MODULE_NAME}_HEADERS})
        set_target_properties("${TARGET_NAME}" PROPERTIES PUBLIC_HEADER "${HEADER}")
    endforeach()

    if(EXISTS "${MODULE_ROOT}/include")
        if(MODULE_EXTERNAL)
            target_include_directories("${MODULE_NAME}" SYSTEM ${TARGET_SCOPE} "${MODULE_ROOT}/include")
        else()
            target_include_directories("${MODULE_NAME}" ${TARGET_SCOPE} "${MODULE_ROOT}/include")
        endif()
    endif()

    if(NOT "${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "INLINE")
        foreach(GROUP ${${MODULE_NAME}_SOURCE_GROUPS})
            target_include_directories("${MODULE_NAME}" PRIVATE "${MODULE_ROOT}/${GROUP}")
        endforeach()
    endif()

    if(NOT "${${MODULE_NAME}_COMPILE_OPTIONS}" STREQUAL "")
        target_compile_options("${TARGET_NAME}" ${TARGET_SCOPE} ${${MODULE_NAME}_COMPILE_OPTIONS})
    endif()

    if(NOT "${${MODULE_NAME}_LINK_OPTIONS}" STREQUAL "")
        target_link_options("${TARGET_NAME}" ${TARGET_SCOPE} ${${MODULE_NAME}_LINK_OPTIONS})
    endif()

    if(NOT "${${MODULE_NAME}_PROPERTIES}" STREQUAL "")
        set_target_properties("${TARGET_NAME}" PROPERTIES ${${MODULE_NAME}_PROPERTIES})
    endif()

    #--------------------------------------------------------
    # Dependencies

    if(NOT "${${MODULE_NAME}_MODULE_TYPE}" STREQUAL "INLINE")
        if("${CMAKE_GENERATOR}" STREQUAL "Xcode" AND(DEFINED ENV{CC} OR DEFINED ENV{CXX}))
            set_target_properties("${TARGET_NAME}" PROPERTIES XCODE_ATTRIBUTE_CC "$ENV{CC}")
            set_target_properties("${TARGET_NAME}" PROPERTIES XCODE_ATTRIBUTE_CXX "$ENV{CXX}")
            set_target_properties("${TARGET_NAME}" PROPERTIES XCODE_ATTRIBUTE_COMPILER_INDEX_STORE_ENABLE NO)
        endif()

        set_target_properties("${TARGET_NAME}" PROPERTIES LINKER_LANGUAGE CXX)

        if(NOT "${${MODULE_NAME}_API_DOMAIN}" STREQUAL "")
            set(module ${${MODULE_NAME}_API_DOMAIN})
        else()
            name_lower(module ${MODULE_NAME})
        endif()

        target_compile_definitions("${TARGET_NAME}" PRIVATE "asd_${module}_api=1")
    endif()

    import_predefined_dependencies("${MODULE_NAME}" "${TARGET_NAME}")

    if(NOT "${${MODULE_NAME}_INCLUDE_DIRECTORIES}" STREQUAL "")
        if(MODULE_EXTERNAL)
            target_include_directories("${MODULE_NAME}" SYSTEM ${TARGET_SCOPE} ${${MODULE_NAME}_INCLUDE_DIRECTORIES})
        else()
            target_include_directories("${MODULE_NAME}" ${TARGET_SCOPE} ${${MODULE_NAME}_INCLUDE_DIRECTORIES})
        endif()
    endif()

    if(NOT "${${MODULE_NAME}_SYSTEM_INCLUDE_DIRECTORIES}" STREQUAL "")
        target_include_directories("${MODULE_NAME}" SYSTEM ${TARGET_SCOPE} ${${MODULE_NAME}_SYSTEM_INCLUDE_DIRECTORIES})
    endif()

    if(NOT "${${MODULE_NAME}_LINK_LIBRARIES}" STREQUAL "")
        target_link_libraries("${MODULE_NAME}" ${TARGET_SCOPE} ${${MODULE_NAME}_LINK_LIBRARIES})
    endif()

    if(NOT "${${MODULE_NAME}_CUSTOM_DEPENDENCIES}" STREQUAL "")
        add_dependencies("${MODULE_NAME}" ${${MODULE_NAME}_CUSTOM_DEPENDENCIES})
    endif()

    set("${PACKAGE_DIR}_TARGETS" ${${PACKAGE_DIR}_TARGETS} ${MODULE_NAME} CACHE INTERNAL "" FORCE)
endfunction()

function(endmodule)
    if("${MODULE_NAME}" STREQUAL "")
        message(FATAL_ERROR "Unexpected `endmodule()`, call module() first")
    endif()

    set(SOURCES ${${MODULE_NAME}_SOURCES})
    module_register_files(${SOURCES})

    if(REGENERATE_PACKAGES)
        add_custom_target("${MODULE_NAME}")
    else()
        setup_module_target("${MODULE_NAME}")
    endif()

    #--------------------------------------------------------
    # Cleanup

    unset(CURRENT_SOURCE_DOMAIN CACHE)
    unset(CURRENT_SOURCE_GROUP_PATH CACHE)
    unset(CURRENT_SOURCE_GROUP_NAME CACHE)

    unset(${MODULE_NAME}_CUSTOM_DEPENDENCIES CACHE)
    unset(${MODULE_NAME}_SOURCES CACHE)
    unset(${MODULE_NAME}_HEADERS CACHE)
    unset(${MODULE_NAME}_SOURCE_GROUPS CACHE)
    unset(${MODULE_NAME}_TASKS CACHE)
    unset(${MODULE_NAME}_MODULE_TYPE CACHE)
    unset(${MODULE_NAME}_INCLUDE_DIRECTORIES CACHE)
    unset(${MODULE_NAME}_SYSTEM_INCLUDE_DIRECTORIES CACHE)
    unset(${MODULE_NAME}_LINK_LIBRARIES CACHE)
    unset(${MODULE_NAME}_COMPILE_OPTIONS CACHE)
    unset(${MODULE_NAME}_LINK_OPTIONS CACHE)
    unset(${MODULE_NAME}_PROPERTIES CACHE)
    unset(${MODULE_NAME}_FOLDER CACHE)
    unset(${MODULE_NAME}_API_DOMAIN CACHE)
endfunction()

function(check_unlisted_files)
    set(UNLISTED_FILES)

    foreach(SOURCE_GROUP IN LISTS DIRECTORY_SOURCE_GROUPS)
        file(GLOB_RECURSE FILES LIST_DIRECTORIES FALSE "${SOURCE_GROUP}/*")

        foreach(FILE IN LISTS FILES)
            if(NOT "${FILE}" IN_LIST DIRECTORY_REGISTERED_FILES)
                list(APPEND UNLISTED_FILES "${FILE}")

                if(REMOVE_UNLISTED_FILES)
                    color_message(RED BOLD "${MESSAGE_REMOVE_STATUS} " RESET "Remove unlisted file: " BOLD "${FILE}")
                    file(REMOVE "${FILE}")
                else()
                    color_message(YELLOW BOLD "${MESSAGE_WARNING_STATUS} " RESET "Found unlisted file: " YELLOW BOLD "${FILE}")
                endif()
            endif()
        endforeach()
    endforeach()

    unset(DIRECTORY_SOURCE_GROUPS CACHE)
    unset(DIRECTORY_REGISTERED_FILES CACHE)

    set(ALL_UNLISTED_FILES ${ALL_UNLISTED_FILES} ${UNLISTED_FILES} CACHE INTERNAL "" FORCE)

    list(LENGTH UNLISTED_FILES UNLISTED_FILES_COUNT)

    if(${UNLISTED_FILES_COUNT} EQUAL 1)
        if(REMOVE_UNLISTED_FILES)
            color_message(WHITE "   1 unlisted file removed")
        else()
            color_message(WHITE "   1 unlisted file found")
        endif()
    elseif(NOT ${UNLISTED_FILES_COUNT} EQUAL 0)
        if(REMOVE_UNLISTED_FILES)
            color_message(WHITE "   ${UNLISTED_FILES_COUNT} unlisted files removed")
        else()
            color_message(WHITE "   ${UNLISTED_FILES_COUNT} unlisted files found")
        endif()
    endif()
endfunction()

function(report_all_unlisted_files)
    set(UNLISTED_FILES ${ALL_UNLISTED_FILES})
    list(REMOVE_DUPLICATES UNLISTED_FILES)

    unset(ALL_UNLISTED_FILES CACHE)

    list(LENGTH UNLISTED_FILES UNLISTED_FILES_COUNT)

    if(${UNLISTED_FILES_COUNT} EQUAL 0)
        color_message(GREEN BOLD "${MESSAGE_SUCCESS_STATUS} " RESET "No unlisted files found in any given package")
    elseif(${UNLISTED_FILES_COUNT} EQUAL 1)
        if(REMOVE_UNLISTED_FILES)
            color_message(RED BOLD "${MESSAGE_REMOVE_STATUS} " RESET "1 unlisted file removed in total:")
        else()
            color_message(YELLOW BOLD "${MESSAGE_WARNING_STATUS} " RESET "1 unlisted file found in total:")
        endif()
    else()
        if(REMOVE_UNLISTED_FILES)
            color_message(RED BOLD "${MESSAGE_REMOVE_STATUS} " RESET "${UNLISTED_FILES_COUNT} unlisted files removed in total:")
        else()
            color_message(YELLOW BOLD "${MESSAGE_WARNING_STATUS} " RESET "${UNLISTED_FILES_COUNT} unlisted files found in total:")
        endif()
    endif()

    foreach(FILE IN LISTS UNLISTED_FILES)
        color_message(YELLOW BOLD "   ${FILE}")
    endforeach()

    message("")
endfunction()
