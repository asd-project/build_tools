#!/usr/bin/env bash
root=$(dirname "$0")

for arg in "$@"; do
    if [[ "$arg" != "${arg%[[:space:]]*}" ]]; then arg="\""$arg"\""; fi
    args="${args} ${arg}"
done

cmake -DWORKSPACE_DIR=`pwd` -D "ARGS=${args}" -P "$root/cli.cmake"
