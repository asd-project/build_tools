#--------------------------------------------------------
#   ASD assets functions
#--------------------------------------------------------

function(assets_source name path)
    if("${MODULE_NAME}" STREQUAL "")
        message(FATAL_ERROR "Unexpected `assets_source()`, call module() first")
    endif()

    set(options NO_RECURSE)
    set(single_value DESTINATION GLOB)
    set(multi_value EXCLUDE)

    cmake_parse_arguments(ASSETS "${options}" "${single_value}" "${multi_value}" ${ARGN})

    set(ASSETS_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${path}")

    if(NOT EXISTS "${ASSETS_PATH}")
        color_message(BOLD GREEN "${MESSAGE_ADD_STATUS} " RESET "Create assets folder: " BOLD "${ASSETS_PATH}")
        file(MAKE_DIRECTORY "${ASSETS_PATH}")
    endif()

    if(ASSETS_DESTINATION)
        set(OUTPUT_DIR "${${MODULE_NAME}_BINARY_OUTPUT}/${ASSETS_DESTINATION}")
        set(EMSCRIPTEN_ROOT "/${ASSETS_DESTINATION}/")
    else()
        set(OUTPUT_DIR "${${MODULE_NAME}_BINARY_OUTPUT}")
        set(EMSCRIPTEN_ROOT "/")
    endif()

    if(ASSETS_NO_RECURSE)
        set(GLOB_EXPR GLOB)
    else()
        set(GLOB_EXPR GLOB_RECURSE)
    endif()

    if(ASSETS_GLOB)
        file(${GLOB_EXPR} assets_files LIST_DIRECTORIES false CONFIGURE_DEPENDS RELATIVE "${ASSETS_PATH}" "${ASSETS_PATH}/${ASSETS_GLOB}")
    else()
        file(${GLOB_EXPR} assets_files LIST_DIRECTORIES false CONFIGURE_DEPENDS RELATIVE "${ASSETS_PATH}" "${ASSETS_PATH}/*")
    endif()

    set(OUTPUT_FILES)

    foreach(file ${assets_files})
        set(INPUT_FILE "${ASSETS_PATH}/${file}")

        set(EXCLUDE OFF)

        foreach(EXCLUDED ${ASSETS_EXCLUDE})
            if("${INPUT_FILE}" MATCHES "^${ASSETS_PATH}/${EXCLUDED}")
                set(EXCLUDE ON)
                break()
            endif()
        endforeach()

        if(EXCLUDE)
            continue()
        endif()

        set(OUTPUT_FILE "${OUTPUT_DIR}/${file}")

        get_filename_component(dir "${file}" DIRECTORY)
        string(REGEX REPLACE "^\./" "" dir "${dir}")
        string(REGEX REPLACE "/$" "" dir "${dir}")
        string(REPLACE "/" "\\" dir "${dir}")

        if("${dir}" STREQUAL "")
            source_group(Assets FILES "${INPUT_FILE}")
        else()
            source_group(Assets\\${dir} FILES "${INPUT_FILE}")
        endif()

        set(${MODULE_NAME}_SOURCES ${${MODULE_NAME}_SOURCES} "${INPUT_FILE}" CACHE INTERNAL "${MODULE_NAME} sources" FORCE)

        add_custom_command(
            OUTPUT "${OUTPUT_FILE}" DEPENDS "${INPUT_FILE}"
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${INPUT_FILE}" "${OUTPUT_FILE}"
            VERBATIM
        )

        list(APPEND OUTPUT_FILES "${OUTPUT_FILE}")

        emscripten_options("--preload-file ${OUTPUT_FILE}@${EMSCRIPTEN_ROOT}${file}")
    endforeach()

    if(EMSCRIPTEN)
        module_link_files("${name}_files" ${OUTPUT_FILES})
    endif()

    add_custom_target("${MODULE_NAME}_${name}" DEPENDS ${OUTPUT_FILES})
    set_target_properties("${MODULE_NAME}_${name}" PROPERTIES FOLDER "tasks")
    module_custom_dependencies("${MODULE_NAME}_${name}")
endfunction()
