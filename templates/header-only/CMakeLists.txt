#--------------------------------------------------------
#   package @package_name@
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include(bootstrap)
include(module)

#--------------------------------------------------------

project(@package_name@ VERSION 0.0.1)

#--------------------------------------------------------

module(INLINE)
    domain(@package_name@)

    group(include Headers)
    files(
        @package_name@.h
    )
endmodule()

#--------------------------------------------------------
