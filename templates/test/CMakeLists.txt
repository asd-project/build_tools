#--------------------------------------------------------
#   package @package_name@
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include(bootstrap)
include(module)

#--------------------------------------------------------

project(@package_name@ VERSION 0.0.1)

#--------------------------------------------------------

module(TEST @package_name@)
    domain(app)

    group(src Sources)
    files(
        main.test.cpp
    )
endmodule()

#--------------------------------------------------------
