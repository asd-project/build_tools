//---------------------------------------------------------------------------

#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <launch/main.h>

//---------------------------------------------------------------------------

namespace ${namespace}
{
    TEST_CASE("<TEST_CASE HEADER>", "[${namespace}]") {
        REQUIRE(1 + 1 == 2);
    }

    static ::asd::entrance open([](int argc, char * argv[]) {
        Catch::Session session;

        if (int return_code = session.applyCommandLine(argc, argv); return_code != 0) {
            return return_code;
        }

        return session.run();
    });
}

//---------------------------------------------------------------------------
