
import os

from conans import ConanFile, CMake, tools

project_name = "@package_name@"


class @PackageName@Conan(ConanFile):
    name = project_name
    version = "0.0.1"
    url = "@package_url@"
    topics = ()
    generators = "cmake"
    no_copy_source = True

    def source(self):
        self.run("git clone @package_url@ @package_name@")

    def package(self):
        self.copy("*.hpp", dst="include", src="@package_name@/include")

    def package_info(self):
        if not self.in_local_cache:
            self.cpp_info.includedirs = ["@package_name@/include"]
