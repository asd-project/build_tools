#include <benchmark/benchmark.h>

#include <cmath>

static void pow(benchmark::State& state) {
    float x = rand();

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = std::pow(x, 3.0f));
    }
}

BENCHMARK(pow);

BENCHMARK_MAIN();
