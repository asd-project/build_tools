
if(APPLE AND "${CMAKE_HOST_SYSTEM_NAME}" STREQUAL "${CMAKE_SYSTEM_NAME}")
    function(osx_framework name)
        find_library(
            FRAMEWORK_${name} NAMES ${name}
            PATHS ${CMAKE_OSX_SYSROOT}/System/Library
            PATH_SUFFIXES Frameworks NO_DEFAULT_PATH
        )

        if(${FRAMEWORK_${name}} STREQUAL FRAMEWORK_${name}-NOTFOUND)
            message(FATAL_ERROR "Framework ${name} not found")
        endif()

        module_link_libraries("${FRAMEWORK_${name}}")
        color_message(GREEN "   Framework ${name} found at ${FRAMEWORK_${name}}")
    endfunction()
else()
    function(osx_framework)
    endfunction()
endif()
