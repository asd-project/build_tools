
if("${CMAKE_SYSTEM_NAME}" STREQUAL "Emscripten")
    if(NOT "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        message(FATAL_ERROR "Emscripten builds with a compiler other than Clang are not supported")
    endif()

    macro(emscripten_options)
        set(OPTIONS)

        foreach(opt ${ARGN})
            list(APPEND OPTIONS "SHELL:${opt}")
        endforeach()

        module_compile_options(${OPTIONS})
        module_link_options(${OPTIONS})
    endmacro()

    macro(emscripten_shell_file PATH)
        set(FILE "${CMAKE_CURRENT_SOURCE_DIR}/${PATH}")

        module_link_files(shell_file "${FILE}")
        emscripten_options("--shell-file \"${FILE}\"")

        set(${MODULE_NAME}_SOURCES ${${MODULE_NAME}_SOURCES} "${FILE}" CACHE INTERNAL "${MODULE_NAME} sources" FORCE)
        source_group(Assets FILES "${FILE}")
    endmacro()

    set(EMSCRIPTEN ON)
else()
    macro(emscripten_options)
    endmacro()

    macro(emscripten_shell_file)
    endmacro()
endif()
