include(commands/create)
include(commands/build)

function(test_build_packages)
    create(LIBRARY base_package)
    assert(EXISTS "${WORKSPACE_DIR}/base_package")

    test_configure_packages("${WORKSPACE_DIR}/base_package")
    assert(EXISTS "${WORKSPACE_DIR}/base_package/src/base_package/base_package.cpp")

    build("${CMAKE_BINARY_DIR}" "Release" "base_package")
    assert(EXISTS "${CMAKE_BINARY_DIR}/asd.base_package/lib/base_package.lib")
endfunction()

testcase(test_build_packages "Build packages")
