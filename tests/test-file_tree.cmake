# set(FILE_TREE_TRACE ON)
# set(FILE_TREE_DRY_RUN ON)

include(utils/file_tree)
include(commands/build)

function(test_file_tree_separate_includes)
    file_tree(APP
        # Add files from the following subtree into list variable APP_SOURCES.
        #
        # `+` is the command that will be executed for each nested node in the given tree and
        # add its path into the provided context variable.
        # `|` marks the start of the filter expression which determine whether to apply the command
        # to a tree node.
        #
        # Filter expressions consist of a filter command and filter arguments. Typical commands are
        # `TYPE` and `MATCHES`.
        # The `TYPE` command receives one arguments which possible values are "FILE" or "DIRECTORY".
        # It also has two aliases `FILE` and `DIRECTORY` which expand into `TYPE FILE` and
        # `TYPE DIRECTORY` respectively.
        # The `MATCHES` command acts exactly as the MATCHES operator in cmake `if` statements.
        [ + SOURCES | FILE ]
        "${WORKSPACE_DIR}" {
            # `source_root` is an alias for `cfg source_root`.
            # The `cfg` commands does nothing more than setting config variables to the provided
            # values. Main differences from a simple assignment are following:
            # - The name of a resulting variable is constructed from the "FILE_CFG_" prefix and
            #   uppercased version of a provided name.
            # - Resulting variable is available only for the nested nodes and not propagated to the
            #   scope where `file_tree` function was called
            #
            # This command sets FILE_CFG_SOURCE_ROOT config variable to the directory path.
            #
            # This path will be used by the `file_cfg` utility as a base path for corresponding
            # header files paths inserted into generated .cpp files and to construct namespaces
            # based on the names of intermediate directories.
            [ source_root . ]
            src {
                # `namespace` is an alias for `cfg namespace`
                #
                # Force using the given namespace when configuring files in the following subtree
                # (FILE_CFG_NAMESPACE=app).
                [ namespace app ]
                app {
                    # `template` is an alias for `cfg template`
                    #
                    # Command sets FILE_CFG_TEMPLATE config variable to the provided value
                    # "main.cpp".
                    #
                    # It instructs underlying `file_cfg` utility to use template file "main.cpp" to
                    # configure the following file if it doesn't exist yet.
                    #
                    # It is optional, as template files can be implicitly selected by matching
                    # extensions, exact names, or even parent directories + extensions.
                    [ template "main.cpp" ]
                    main.cpp

                    misc {
                        foo.cpp
                    }
                }
            }
        }
    )

    file_tree(APP
        # Add files from the following subtree into list variable APP_SOURCES.
        [ + SOURCES | FILE ]
        "${WORKSPACE_DIR}" {
            [ + HEADERS | FILE ]
            [ = INCLUDE_ROOT ]
            [ source_root . ]
            include {
                app {
                    config.h

                    [ template "_.h" ]
                    . { # specify commands for a group of files without creating subdirectory
                        control.h
                        gui.h
                    }

                    misc {
                        foo.h
                        bar.h
                    }
                }
            }
        }
    )

    assert("${FILE_CFG_VARIABLES}" STREQUAL "") # internal variables are not propagated to the current scope

    assert(DEFINED APP_SOURCES)

    set(EXPECTED_SOURCE_FILES src/app/main.cpp src/app/misc/foo.cpp)
    set(EXPECTED_HEADER_FILES include/app/config.h include/app/control.h include/app/gui.h include/app/misc/foo.h include/app/misc/bar.h)

    set(EXPECTED_SOURCES)

    foreach(FILE ${EXPECTED_SOURCE_FILES} ${EXPECTED_HEADER_FILES})
        list(APPEND EXPECTED_SOURCES "${WORKSPACE_DIR}/${FILE}")
    endforeach()

    list(SORT APP_SOURCES)
    list(SORT EXPECTED_SOURCES)
    assert("${APP_SOURCES}" STREQUAL "${EXPECTED_SOURCES}")

    assert(DEFINED APP_HEADERS)

    set(EXPECTED_HEADERS)

    foreach(FILE ${EXPECTED_HEADER_FILES})
        list(APPEND EXPECTED_HEADERS "${WORKSPACE_DIR}/${FILE}")
    endforeach()

    list(SORT APP_HEADERS)
    list(SORT EXPECTED_HEADERS)
    assert("${APP_HEADERS}" STREQUAL "${EXPECTED_HEADERS}")

    assert("${APP_INCLUDE_ROOT}" STREQUAL "${WORKSPACE_DIR}/include")
endfunction()

function(test_file_tree_combined_includes)
    file_tree(APP
        # Add files from the following subtree into list variable SOURCES.
        [ + SOURCES | FILE ]
        [ + HEADERS | MATCHES ".h$" ]
        "${WORKSPACE_DIR}" {
            [ source_root . ]
            src {
                app {
                    config.h

                    [ template "_.h" ]
                    . {
                        control.h
                        gui.h
                    }

                    [ template "main.cpp" ]
                    main.cpp

                    misc {
                        foo.h
                        foo.cpp
                        bar.h
                    }
                }
            }
        }
    )

    assert("${FILE_CFG_VARIABLES}" STREQUAL "") # internal variables are not propagated to the current scope

    assert(DEFINED APP_SOURCES)

    set(EXPECTED_SOURCE_FILES src/app/main.cpp src/app/misc/foo.cpp)
    set(EXPECTED_HEADER_FILES src/app/config.h src/app/control.h src/app/gui.h src/app/misc/foo.h src/app/misc/bar.h)

    set(EXPECTED_SOURCES)

    foreach(FILE ${EXPECTED_SOURCE_FILES} ${EXPECTED_HEADER_FILES})
        list(APPEND EXPECTED_SOURCES "${WORKSPACE_DIR}/${FILE}")
    endforeach()

    list(SORT APP_SOURCES)
    list(SORT EXPECTED_SOURCES)
    assert("${APP_SOURCES}" STREQUAL "${EXPECTED_SOURCES}")

    assert(DEFINED APP_HEADERS)

    set(EXPECTED_HEADERS)

    foreach(FILE ${EXPECTED_HEADER_FILES})
        list(APPEND EXPECTED_HEADERS "${WORKSPACE_DIR}/${FILE}")
    endforeach()

    list(SORT APP_HEADERS)
    list(SORT EXPECTED_HEADERS)
    assert("${APP_HEADERS}" STREQUAL "${EXPECTED_HEADERS}")
endfunction()

function(test_file_tree_configure_and_build)
    set(CMAKE_FILE_TEXT [[

cmake_minimum_required(VERSION 3.19)

project(file_tree_test)

include("${BUILD_TOOLS}/setup.cmake")
include(utils/file_tree)

file_tree(APP
    [ + SOURCES | FILE ]
    [ + HEADERS | MATCHES ".h$" ]
    "${WORKSPACE_DIR}" {
        [ = INCLUDE_DIR ]
        [ source_root . ]
        src {
            app {
                config.h

                [ template "_.h" ]
                . {
                    control.h
                    gui.h
                }

                [ template "main.cpp" ]
                main.cpp

                misc {
                    foo.h
                    foo.cpp
                    bar.h
                }
            }
        }
    }
)

add_executable(\${PROJECT_NAME} \${APP_SOURCES})

target_compile_features(\${PROJECT_NAME} PUBLIC cxx_std_17)
target_include_directories(\${PROJECT_NAME} PUBLIC "\${APP_INCLUDE_DIR}")

]])

    test_configure_from_text("${CMAKE_FILE_TEXT}")

    assert(EXISTS "${WORKSPACE_DIR}/src/app/main.cpp")

    build("${CMAKE_BINARY_DIR}" "Release" TARGET "file_tree_test")
endfunction()

testcase(test_file_tree_separate_includes "Create tree with separate includes")
testcase(test_file_tree_combined_includes "Create tree with combined includes")
testcase(test_file_tree_configure_and_build "Configure and build project with file_tree sources")
