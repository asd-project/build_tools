include(commands/create)

function(test_create_packages)
    create(LIBRARY base_package)
    assert(EXISTS "${WORKSPACE_DIR}/base_package")

    create(TEST test_package BASE base_package)
    assert(EXISTS "${WORKSPACE_DIR}/base_package/tests/test_package")

    create(TEST separate_test)
    assert(EXISTS "${WORKSPACE_DIR}/separate_test")

    test_configure_packages("${WORKSPACE_DIR}/base_package" "${WORKSPACE_DIR}/separate_test")

    assert(EXISTS "${WORKSPACE_DIR}/base_package/src/base_package/base_package.cpp")
    assert(EXISTS "${WORKSPACE_DIR}/base_package/tests/test_package/src/app/main.test.cpp")
    assert(EXISTS "${WORKSPACE_DIR}/separate_test/src/app/main.test.cpp")
endfunction()

testcase(test_create_packages "Create packages")
